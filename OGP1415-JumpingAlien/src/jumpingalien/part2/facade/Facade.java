package jumpingalien.part2.facade;

import java.util.Collection;

import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.util.ModelException;
import jumpingalien.util.Sprite;

public class Facade implements IFacadePart2{
	IFacade facadePart1 = new jumpingalien.part1.facade.Facade();

	@Override
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite[] sprites) {
		return facadePart1.createMazub(pixelLeftX, pixelBottomY, sprites);
	}

	@Override
	public int[] getLocation(Mazub alien) {
		return facadePart1.getLocation(alien);
	}

	@Override
	public double[] getVelocity(Mazub alien) {
		return facadePart1.getVelocity(alien);
	}

	@Override
	public double[] getAcceleration(Mazub alien) {
		return facadePart1.getAcceleration(alien);
	}

	@Override
	public int[] getSize(Mazub alien) {
		return facadePart1.getSize(alien);
	}

	@Override
	public Sprite getCurrentSprite(Mazub alien) {
		return facadePart1.getCurrentSprite(alien);
	}

	@Override
	public void startJump(Mazub alien) {
		facadePart1.startJump(alien);
	}

	@Override
	public void endJump(Mazub alien) {
		facadePart1.endJump(alien);
	}

	@Override
	public void startMoveLeft(Mazub alien) {
		facadePart1.startMoveLeft(alien);
	}

	@Override
	public void endMoveLeft(Mazub alien) {
		facadePart1.endMoveLeft(alien);
	}

	@Override
	public void startMoveRight(Mazub alien) {
		facadePart1.startMoveRight(alien);
	}

	@Override
	public void endMoveRight(Mazub alien) {
		facadePart1.endMoveRight(alien);
	}

	@Override
	public void startDuck(Mazub alien) {
		facadePart1.startDuck(alien);
	}

	@Override
	public void endDuck(Mazub alien) {
		facadePart1.endDuck(alien);
	}

	@Override
	public int getNbHitPoints(Mazub alien) {
		return alien.getNbHitPoints();
	}

	@Override
	public World createWorld(int tileSize, int nbTilesX, int nbTilesY,
			int visibleWindowWidth, int visibleWindowHeight, int targetTileX,
			int targetTileY) {
		return new World(tileSize, nbTilesX, nbTilesY, visibleWindowWidth, visibleWindowHeight, targetTileX, targetTileY);
	}

	@Override
	public int[] getWorldSizeInPixels(World world) {
		int[] size = new int[2];
		size[0] = world.getGameWorldWidth();
		size[1] = world.getGameWorldHeigth();
		return size;
	}

	@Override
	public int getTileLength(World world) {
		return world.getTileSize();
	}

	@Override
	public void startGame(World world) { 
		world.setGameStarted(true);
	}

	@Override
	public boolean isGameOver(World world) {
		return world.isGameOver();
	}

	@Override
	public boolean didPlayerWin(World world) {
		return world.didPlayerWin();
	}

	@Override
	public void advanceTime(World world, double dt) {
		world.advanceTime(dt);
	}

	@Override
	public int[] getVisibleWindow(World world) {
		int size[] = world.getVisibleWindow();
		return size;
	}

	@Override
	public int[] getBottomLeftPixelOfTile(World world, int tileX, int tileY) {
		int[] pixel = world.getBottomLeftPixelOfTile(tileX, tileY);
		return pixel;
	}

	@Override
	public int[][] getTilePositionsIn(World world, int pixelLeft,
			int pixelBottom, int pixelRight, int pixelTop) {
		int[][] positions = world.getTilePositionsIn(pixelLeft, pixelBottom, pixelRight, pixelTop);
		return positions;
	}

	@Override
	public int getGeologicalFeature(World world, int pixelX, int pixelY)
			throws ModelException {
		int feature = world.getGeologicalFeature(pixelX, pixelY);
		return feature;
	}

	@Override
	public void setGeologicalFeature(World world, int tileX, int tileY,
			int tileType) {
		world.setGeologicalFeature(tileX, tileY, tileType);
	}

	@Override
	public void setMazub(World world, Mazub alien) {
		world.setMazub(alien);
	}

	@Override
	public boolean isImmune(Mazub alien) { 
		return alien.isImmune();
	}

	@Override
	public Plant createPlant(int x, int y, Sprite[] sprites) {
		return new Plant(x, y, sprites);
	}

	@Override
	public void addPlant(World world, Plant plant) {
		world.addPlant(plant);
	}

	@Override
	public Collection<Plant> getPlants(World world) {
		return world.getPlants();
	}

	@Override
	public int[] getLocation(Plant plant) {
		int[] position = new int[2];
		position[0] = (int) plant.getCurrentHorizontalPosition();
		position[1] = (int) plant.getCurrentVerticalPosition();
		return position;
	}

	@Override
	public Sprite getCurrentSprite(Plant plant) {
		return plant.getCurrentSprite();
	}

	@Override
	public Shark createShark(int x, int y, Sprite[] sprites) {
		return new Shark(x, y, sprites);
	}

	@Override
	public void addShark(World world, Shark shark) {
		world.addShark(shark);
	}

	@Override
	public Collection<Shark> getSharks(World world) {
		return world.getSharks();
	}

	@Override
	public int[] getLocation(Shark shark) {
		int[] position = new int[2];
		position[0] = (int) shark.getCurrentHorizontalPosition();
		position[1] = (int) shark.getCurrentVerticalPosition();
		return position;
	}

	@Override
	public Sprite getCurrentSprite(Shark shark) {
		return shark.getCurrentSprite();
	}

	@Override
	public School createSchool() {
		return new School();
	}

	@Override
	public Slime createSlime(int x, int y, Sprite[] sprites, School school) {
		return new Slime(x, y, sprites, school);
	}

	@Override
	public void addSlime(World world, Slime slime) {
		world.addSlime(slime);
	}

	@Override
	public Collection<Slime> getSlimes(World world) {
		return world.getSlimes();
	}

	@Override
	public int[] getLocation(Slime slime) {
		int[] position = new int[2];
		position[0] = (int) slime.getCurrentHorizontalPosition();
		position[1] = (int) slime.getCurrentVerticalPosition();
		return position;
	}

	@Override
	public Sprite getCurrentSprite(Slime slime) {
		return slime.getCurrentSprite();
	}

	@Override
	public School getSchool(Slime slime) {
		return slime.getSchool();
	}

}
