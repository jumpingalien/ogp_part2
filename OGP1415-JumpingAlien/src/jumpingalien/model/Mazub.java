package jumpingalien.model;

import java.util.List;

import jumpingalien.util.Sprite;
import jumpingalien.util.Util;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class for dealing with the player's character Mazub.
 * 
 * @version	2.0
 * @author  Bram Vandendriessche
 * @author	Maaike Van Roy
 *
 */


public class Mazub extends GameObject{
	

	/**
	 * Initialize a new Mazub on a given start position and display it using an array of sprite images.
	 * 
	 * @param 	pixelX
	 * 		  	The initial X position for this new Mazub.
	 * @param 	pixelY
	 * 		  	The initial Y position for this new Mazub.
	 * @param 	sprites
	 * 		  	The array of sprite images for this new Mazub.
	 * @effect	Specific values are given to the constructor of the super class, including accelerations, positions, hitpoints etc.
	 * 			| super(pixelX, pixelY, sprites, 100, 3, 1, 8, .9, -10, 500, 0.6)
	 * @effect  The passed time is set to value 1
	 * 			| setPassedTime(1)
	 * @post	The ducking velocity is initialized on 1.
	 * 			| new.getDuckingVelocity() == 1
	 * @post	The variable m is initialized with the number of sprite images to the left and right.
	 * 			| new.getM() == (sprites.length - 10)/2
	 * @post	The current maximum horizontal velocity is set to the initial maximum horizontal velocity.
	 * 			| new.getCurrentMaximumHorizontalVelocity == getInitialMaximumHorizontalVelocity
	 */
	public Mazub(int pixelX, int pixelY, Sprite[] sprites){
		super(pixelX, pixelY, sprites, 100, 3, 1, 8, .9, -10, 500, 0.6);
		this.setPassedTime(1);
		this.duckingVelocity = 1;
		m = (sprites.length - 10) / 2;
		this.currentMaximumHorizontalVelocity = getInitialMaximumHorizontalVelocity();	
	}
	
	
	
	// #########################################                 HITPOINTS AND COLLISIONS
	
	/**
	 * Set the hitpoints of the game object to a given amount.
	 *
	 * @post The number of hitpoints is set to the given amount if this amount is valid.
	 * 		| if(isValidAmountOfHitPoinst(amount) {
	 * 		| 		new.getNbHitPoints() == amount
	 * 		| }
	 * @post The game stops and the player loses if the amount of hitpoints is less than or equal to zero.
	 * 		| else if(amount <= 0) {
	 * 		|		new.getNbHitPoints() == 0
	 * 		| 		new.isTerminated() == true
	 * 		|		new.isGameOver() == true
	 * 		|		new.didPlayerWin() == false
	 * 		| }
	 */
	@Override
	protected void setNbHitPoints(int amount){
		if (isValidAmountOfHitpoints(amount)){
			this.nbHitPoints = amount;
		}
		else if (amount <= 0){
			this.nbHitPoints = 0;
			this.setTerminated(true);
			getWorld().setGameOver(true);
			getWorld().setPlayerWin(false);
		}
	}
	
	/**
	 * Applies eventual collisions of Mazub with his environment.
	 * 
	 * @effect	| if (getFeatureTime() == -1)
	 * 			|	if (feat == 3)
	 * 			|		setFeatureTime(dt);
	 *   		|	    setNbHitPoints(getNbHitPoints() - 50)
	 * 			|		noDamage = false
	 * 			|   else if (feat == 2) 
	 *			|       setFeatureTime(dt)
	 *			|       noDamage = false
	 *			| else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2))
	 *			| 	for each feat in featureList
	 *			|     	if (feat == 2)
	 *       	|			setNbHitPoints(getNbHitPoints() - 2)
	 *       	|			setFeatureTime(getFeatureTime() - .2 + dt)
	 *       	|			noDamage = false
	 *          | else 
	 *			|   setFeatureTime(getFeatureTime() + dt)
	 *			|   noDamage = false
	 * @effect	| if (noDamage == true) 
	 *			|   setFeatureTime(-1)
	 */
	@Override
	protected void applyPassableAreaCollisions(double dt) {
	  List<Integer> featureList = this.getWorld().passableAreaCollisionList(this);
	  //3 is magma
	  //2 is water
	  //1 is solid
	  //0 is lucht
	  boolean noDamage = true;
	  if (getFeatureTime() == -1) {
	    for (Integer feat: featureList) {
	      if (feat == 3) {
	        setFeatureTime(dt);
	        setNbHitPoints(getNbHitPoints() - 50);
	        noDamage = false;
	        break;
	      }
	      else if (feat == 2) {
	        setFeatureTime(dt);
	        noDamage = false;
	        break;
	      }
	    }
	  }
	  
	  else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2)) {

	    for (Integer feat: featureList) {
	      if (feat == 2) {
	        setNbHitPoints(getNbHitPoints() - 2);
	        setFeatureTime(getFeatureTime() - .2 + dt);
	        noDamage = false;
	        break;
	      }
	      else if (feat == 3) {
	        setNbHitPoints(getNbHitPoints() - 50);
	        setFeatureTime(getFeatureTime() - .2 + dt);
	        noDamage = false;
	        break;
	      }
	    }
	  }
	  else {
	    setFeatureTime(getFeatureTime() + dt);
	    noDamage = false;

	  }
	  
	  if (noDamage == true) { 
	    setFeatureTime(-1);
	  }
	}
	
	/**
	 * Applies the effects of a collision between Mazub and another game object.
	 * 
	 * @effect	If the object is a plant, not terminated and fifty points can be counted extra to the hitpoints of Mazub, this is done and the plant loses all of its points
	 * 			|if (object is Plant)
	 * 			|	if (!object.isTerminated() && ((getNbHitPoints() + 50) <= getMaxAmountOfHitpoints()))
	 * 			|		setNbHitPoints(getNbHitPoints() + 50)
	 * 			|		object.setNbHitPoints(0)
	 * @effect	If the object is a slime or a shark and not immune, it loses 50 hitpoints. In case of a slime, its school loses one hitpoint too.
	 * 			| else if (object is Slime or Shark) 
	 * 			| 	if (!object.isImmune()) 
	 * 			| 	object.setNbHitPoints(object.getNbHitPoints() - 50)
	 * 			| 	if (object is Slime) 
	 * 			| 		object.getSchool().collisionEffectOtherSlimes(object)
	 * @effect	If Mazub didn't collide with the other object with his bottom and he is not immune, he loses points too.
	 * 			| if (!bottom && !this.isImmune()) 
	 *			|	setNbHitPoints(getNbHitPoints() - 50)
	 *			|	setImmunity(true)
	 */
	@Override
	protected void applyCollisionEffects(GameObject object, boolean bottom) {
		if (object instanceof Plant){
			if (!object.isTerminated() && ((getNbHitPoints() + 50) <= getMaxAmountOfHitpoints())){
				setNbHitPoints(getNbHitPoints() + 50);
				object.setNbHitPoints(0); 
			}	
		}
		
		else if ((object instanceof Slime || object instanceof Shark)){
			
	
			if (!object.isImmune()){
				object.setNbHitPoints(object.getNbHitPoints() - 50);
				// if slime : all other slimes in same school -1
				if (object instanceof Slime){
					((Slime) object).getSchool().collisionEffectOtherSlimes((Slime) object);
				}
				object.setImmunity(true);
			}
	
			
			if (!bottom && !this.isImmune()){
				setNbHitPoints(getNbHitPoints() - 50);
				setImmunity(true);
			}
		}
	}

	/**
	 * Returns the time that Mazub is placed in an environment feature.
	 */
	private double getFeatureTime() {
		return this.featureTime;
	}

	
	/**
	 * Set the feature time to a specific time.
	 * @param time
	 * 		The time to which the feature time should be set.
	 * @effect The time is set to the given value.
	 * 		| this.featureTime == time
	 */
	private void setFeatureTime(double time) {
		this.featureTime = time;
	}
	
	private double featureTime;

	

	

	// #########################################                 POSITION
	
	/**
	 * Sets the current horizontal position of Mazub to the given horizontal position.
	 * 
	 * @effect	If the given horizontal position is not a valid position and the position is 
	 * 			larger than the maximum allowed horizontal position or smaller than the minimum
	 * 			allowed horizontal position, the player character looses and is terminated, the game stops.
	 * 			| if (! isValidHorizontalPosition(position))
	 * 			|	this.setTerminated(true)
	 * 			|	getWorld().setGameOver(true)
	 * 			|	getWorld().setPlayerWin(false)
	 * @effect	If the horizontal position of the player's character is less than 200 pixels removed from the 
	 * 			visible window's border on the left or right side, the visible window will move simultaneously 
	 * 			with the player's character.
	 * 			| if (position + getWidth() > getVisibleWindowX() + getVisibleWindowWidth - 200)
	 * 			|	setVisibleWindowX(position + getWidth() + 200 - getVisibleWindowWidth())
	 * 			| if (position < getVisibleWindowX() + 200)
	 * 			|	setVisibleWindowX(position - 200)
	 * @effect	The horizontal position is set to the given position.
	 * 			| super.setCurrentHorizontalPosition(position)
	 */
	@Override
	protected void setCurrentHorizontalPosition(double position){
		if (getWorld() != null){
			if (! isValidHorizontalPosition(position)){
				this.setTerminated(true);
				getWorld().setGameOver(true);
				getWorld().setPlayerWin(false);
			}
			
			if ((position + getWidth()) > (getWorld().getVisibleWindowX() + getWorld().getVisibleWindowWidth() - 200)){
					getWorld().setVisibleWindowX(position + getWidth() + 200 - getWorld().getVisibleWindowWidth());
			}
			if ((position < getWorld().getVisibleWindowX() + 200)){
					getWorld().setVisibleWindowX(position-200);
			}
		}
		super.setCurrentHorizontalPosition(position);
	}
	
	/**
	 * Sets the current vertical position of Mazub to the given position.
	 * 
	 * @effect	If the given position is larger than the maximum allowed vertical position or
	 * 			smaller than the minimum allowed vertical position, the player character looses and 
	 * 			is terminated, the game stops.
	 * 			| if (! isValidVerticalPosition(position))
	 * 			|	this.setTerminated(true)
	 * 			|	getWorld().setGameOver(true)
	 * 			|	getWorld().setPlayerWin(false)
	 * @effect	If the vertical position of the player's character is less than 200 pixels removed from the 
	 * 			visible window's border on the top or bottom side, the visible window will move simultaneously 
	 * 			with the player's character.
	 * 			|	if (position + getHeight() > getVisibleWindowY() + getVisibleWindowHeight() - 200)
	 * 			|		setVisibleWindowY(position + getHeight() + 200 - getVisibleWindowHeight())
	 * 			|	if (position < getVisibleWindowY() - 200)
	 * 			|		setVisibleWindow(position - 200) 
	 * @effect	The vertical position is set to the given position.
	 * 			| super.setCurrentVerticalPosition(position)
	 */
	@Override
	protected void setCurrentVerticalPosition(double position){
		if (! isValidVerticalPosition(position)){
			this.setTerminated(true);
			getWorld().setGameOver(true);
			getWorld().setPlayerWin(false);
		}
		
		if (getWorld() != null){
			if (( position + getHeight()) > (getWorld().getVisibleWindowY() + getWorld().getVisibleWindowHeigth() - 200)){
					getWorld().setVisibleWindowY(position+getHeight()+200-getWorld().getVisibleWindowHeigth());
			}
			if (( position < getWorld().getVisibleWindowY() + 200)){
					getWorld().setVisibleWindowY(position-200);
			}
		}
		super.setCurrentVerticalPosition(position);
	}
	
	
	
	

	// 													################  VELOCITY
	


	/**
	 * Return the velocity of the game object when ducking.
	 * 
	 * @return	the game object's velocity while ducking.
	 * 			| result == duckingVelocity
	 */
	@Basic @Immutable
	protected double getHorizontalDuckingVelocity() {
		return this.duckingVelocity;
	}
	
	/**
	 * Return the maximum horizontal velocity that the game object can reach.
	 * 
	 * @return  The highest possible value for the game object's horizontal velocity
	 * 			will never be lower than his initial horizontal velocity.
	 * 			| result >= getInitialHorizontalVelocity()
	 */
	protected double getCurrentMaximumHorizontalVelocity(){
		return this.currentMaximumHorizontalVelocity;
	}

	/**
	 * Sets the maximum horizontal velocity of the game object to the given velocity.
	 * @param 	velocity
	 * 			The new maximum velocity of the game object.
	 * @post	The maximum horizontal velocity of the game object is equal to the 
	 * 			given velocity.
	 * 			| new.getMaximumHorizontalVelocity == velocity
	 */
	protected void setMaximumHorizontalVelocity(double velocity) {
		this.currentMaximumHorizontalVelocity = velocity;
	}

	// #########################################                 POSITION
	
	/**
	 * Returns if the current tile is the target tile for Mazub to win the game.
	 * @return True if and only if Mazub's current position is on the target tile.
	 * 			| result == ( (Math.floor(getCurrentHorizontalPosition()/getWorld().getTileSize())) == getWorld().getTargetTileX() &&
				(Math.floor((getCurrentVerticalPosition()+1)/getWorld().getTileSize())) == getWorld().getTargetTileY()) 
	 */
	private boolean isTargetTile(){
		if ( (Math.floor(getCurrentHorizontalPosition()/getWorld().getTileSize())) == getWorld().getTargetTileX() &&
				(Math.floor((getCurrentVerticalPosition()+1)/getWorld().getTileSize())) == getWorld().getTargetTileY()) 
			return true;
		return false;
	}

	private final double duckingVelocity;
	private double currentMaximumHorizontalVelocity;

	
	
	
	// 														################  SPRITES
	
	





	/**
	 * Returns the sprite image(s) that is/are equivalent with Mazub's movement at that moment.
	 * 
	 * @pre		The given array must be a valid array.
	 * 			| isValidArray(sprites)
	 * @return 	If there is being checked if Mazub can stand up after ducking or Mazub is not moving horizontally, has not moved horizontally within the last second
	 * 			of in-game-time and is not ducking, then return sprite image 0.
	 * @return	If Mazub is not moving horizontally has not moved horizontally within the last second
	 * 			of in-game-time and is ducking, then return sprite image 1.
	 * @return	If Mazub is not moving horizontally but its last horizontal movement was to the right
	 * 			(within 1s), and he is not ducking, then return sprite image 2.
	 * @return	If Mazub is not moving horizontally but its last horizontal movement was to the left
	 * 			(within 1s), and he is not ducking, then return sprite image 3.
	 * @return	If Mazub is moving to the right and jumping and not ducking, then return sprite image 4.
	 * @return	If Mazub is moving to the left and jumping and not ducking, then return sprite image 5.
	 * @return	If Mazub is ducking and moving to the right or was moving to the right (within 1s),
	 * 			then return sprite image 6.
	 * @return	If Mazub is ducking and moving to the left or was moving to the left (within 1s),
	 * 			then return sprite image 7.
	 * @return	If Mazub is neither ducking nor jumping and moving to the right, return the sprite images
	 * 			that are equivalent with the walking cycle.
	 * @return	If Mazub is neither ducking nor jumping and moving to the left, return the sprite images
	 * 			that are equivalent with the walking cycle but mirrored.
	 */
	@Basic @Override
	public Sprite getCurrentSprite(){
		assert super.isValidArray(getArrayOfSprites());
		if (isCheckingGetUp() || (! isMovingLeft() && ! isMovingRight() && getPassedTime() >= 1 && ! isDucking()))
			return getSprite(0);
		else if (! isMovingLeft() && ! isMovingRight() && isDucking() && getPassedTime() >= 1)
			return getSprite(1);
		else if (! isMovingLeft() && ! isMovingRight() && ! isDucking() && getPassedTime() < 1 && hasLastMovedRight())
			return getSprite(2);
		else if (! isMovingLeft() && ! isMovingRight() && ! isDucking() && getPassedTime() < 1 && !hasLastMovedRight())
			return getSprite(3);
		else if ( isMovingRight() && getCurrentVerticalVelocity() != 0 && isJumping() && ! isDucking())
			return getSprite(4);
		else if ( isMovingLeft() && getCurrentVerticalVelocity() != 0 && isJumping() && ! isDucking())
			return getSprite(5);
		else if ( isDucking() && (isMovingRight() || (hasLastMovedRight() && getPassedTime() < 1)) )
			return getSprite(6);
		else if ( isDucking() && (isMovingLeft() || (! hasLastMovedRight() && getPassedTime() < 1))  )
			return getSprite(7);
		else if ( isMovingRight() && ! isDucking() && (! isJumping() || getCurrentVerticalVelocity() == 0))
			return getSprite(getWalkingCycleIndex());
		else if ( isMovingLeft() && ! isDucking() && (! isJumping() || getCurrentVerticalVelocity() == 0))
			return getSprite(getWalkingCycleIndex());
		return null;
	}
	
	
	/**
	 * Return the amount of sprite images that are reserved 
	 * for left or right movement.
	 */
	@Immutable
	public int getM() {
		return this.m;
	}



	/**
	* Sets the last move was to the right state of this the game object
	* according to the given value.
	* 
	* @param 	value
	* 			The new last move was to the right state of
	* 			this the game object.
	* @post	The new last move was to the right state of
	* 			this the game object is equal to the given value.
	* 			| new.hasLastMovedRight() == value
	*/
	public void setLastIsRight(boolean value) {
	this.lastMoveWasRight = value;
	}



		/**
	 * Set the index of the image to be displayed while walking to
	 * the given index.
	 * 
	 * @param 	index
	 * 			The new index of the image to be displayed while walking.
	 * @post	If Mazub is moving to the right and the given index is bigger
	 * 			then the amount of images reserved for moving to the right then
	 * 			the new index is set to the first index of the walking cycle.
	 * 			| if (isMovingRight()) {
	 * 			|	if (index > 8 + getM())
	 * 			|		new.getWalkingCylceIndex() == 8 }
	 * @post	If Mazub is moving to the right and the given index is not bigger 
	 * 			then the amount of images reserved for moving to the right then the
	 * 			new index is set to the given index.
	 * 			| if (isMovingRight()) {
	 * 			|	if (index < 8 + getM())
	 * 			|		new.getWalkingCylceIndex() == index }
	 * @post	If Mazub is moving to the right and the given index is bigger
	 * 			then the amount of images reserved for moving to the right then
	 * 			the new index is set to the first index of the walking cycle.
	 * 			| if (isMovingLeft()) {
	 * 			|	if (index > 9 + 2 * getM())
	 * 			|		new.getWalkingCylceIndex() == 9 + getM() }
	 * @post	If Mazub is moving to the left and the given index is not bigger 
	 * 			then the amount of images reserved for moving to the left then the
	 * 			new index is set to the given index.
	 * 			| if (isMovingLeft()) {
	 * 			|	if (index < 9 + 2 * getM())
	 * 			|		new.getWalkingCylceIndex() == index }
	 */
	public void setWalkingCycleIndex(int index) {
		if (isMovingRight()) {
			if (index > 8 + getM()) {
				this.walkingCycleIndex = 8;
			}
			else
				this.walkingCycleIndex = index;
		}
		if (isMovingLeft()) {
			if (index > 9 + 2 * getM() ) {
				this.walkingCycleIndex = 9 + getM();
			}
			else
				this.walkingCycleIndex = index;
		}
			
	}



		/**
		 * Sets if Mazub's direction has switched to a given flag.
		 * @param flag
		 * 		The flag to which hasSwitchedDirection should be set.
		 * @post hasSwitchedDirection() is set to a new flag.
		 * 		| new.hasSwitchedDirection() == flag
		 */
		public void setHasSwitchedDirection(boolean flag) {
			this.hasSwitchedDirection = flag;
		}



		/**
	 * Check whether the given array is a valid array.
	 * 
	 * @return	False if the length of the array isn't an even number or
	 * 			its length is a number smaller than 10.
	 * 			| result == (array.length < 10) && (array.length % 2 != 0)
	 * @return	True if and only if the length of the array is an even number
	 * 			and its length is a number bigger or equal to 10.
	 * 			| result == (array.length >= 10) && (array.length % 2 == 0)
	 */
	
	@Override	
	protected boolean isValidArray(Sprite[] array){
		return (array.length >= 10) && (array.length % 2 == 0);
	}
	
	
	/**
	 * Returns if Mazub's movement direction just switched.
	 */
	private boolean hasSwitchedDirection() {
		return hasSwitchedDirection;
	}



	/**
	 * Return the time passed since the last movement.
	 */
	private double getPassedTime() {
		return this.passedTime;
	}
	
	/**
	 * Return the index of the image to be displayed when walking.
	 */
	private int getWalkingCycleIndex() {
		return this.walkingCycleIndex;
	}
	
	/**
	 * Return the time passed while moving horizontally.
	 */
	private double getDeltaT2(){
		return this.deltaT_2;
	}

	/**
	 * Set the time passed while moving to the given value.
	 * 
	 * @param 	value
	 * 			The new time that has passed while moving.
	 * @post	The new time that has passed while moving is equal to
	 * 			the given value.
	 * 			| new.getDeltaT2() == value
	 */
	private void setDeltaT2(double value) {
		this.deltaT_2 = value;
	}	
	
	/**
	 * Set the time passed since the last movement to the given time.
	 * 
	 * @param 	time
	 * 			The new time that has passed since the last movement.
	 * @post	The new time that has passed since the last movement is 
	 * 			equal to the given time.
	 * 			| new.getPassedTime == time
	 */
	private void setPassedTime(double time) {
		this.passedTime = time;
	}
	
	private double passedTime;
	private final int m;
	private int walkingCycleIndex;
	private double deltaT_2;

	
	// 														################  MOVEMENT
	
	
	/**
	 * Returns if the keyboard's arrow left is being pressed.
	 */
	public boolean isPressingLeft() {
		return isPressingLeft;
	}

	/**
	 * Returns if the keyboard's right arrow is being pressed.
	 */
	public boolean isPressingRight() {
		return isPressingRight;
	}

	/**
	 * Returns if the keyboard's arrow down is being pressed.
	 */
	public boolean isPressingDown() {
		return this.isPressingDown;
	}

	/**
	 * Check whether Mazub's last movement was to the right.
	 */
	@Basic
	public boolean hasLastMovedRight(){
		return this.lastMoveWasRight;
	}



	/**
	 * 
	 * Mazub starts moving.
	 * 
	 * @effect	The passed time is reset.
	 * 			| setPassedTime(0)
	 * @effect	The current horizontal velocity is set to the initial horizontal velocity.
	 * 			| setHorizontalVelocity(getInitialHorizontalVelocity())
	 * @effect	The current horizontal acceleration is set to the initial horizontal acceleration.
	 * 			| setHorizontalAcceleration(getInitialHorizontalAcceleration())
	 */
	@Override
	public void startMove(){
		this.setPassedTime(0);
		super.startMove();
	}



	/**
	 * Mazub stops moving.
	 * 
	 * @effect	The current horizontal acceleration and current horizontal velocity are set to zero if Mazub is not moving horizontally.
	 * 			| if (!isMovingLeft() && !isMovingRight()) {
	 * 			| 	setHorizontalAcceleration(0)
	 * 			| 	setHorizontalVelocity(0)
	 * 			| }
	 * @effect	Else if the arrow in a direction is pressed and Mazub is moving in this direction and he didn't switch direction, 
	 * 			the current velocity and acceleration should be kept.
	 * 			| if ( (isMovingLeft() && isPressingLeft() || 
	 * 			|		(isMovingRight() && isPressingRight())) && ! hasSwitchedDirection() ) {
	 * 			|	setHorizontalAcceleration(getCurrentHorizontalAcceleration)
	 * 			| 	setHorizontalVelocity(getCurrentHorizontalVelocity)
	 * 			| }
	 * @effect	If none of the two previous conditions are true, the horizontal velocity and acceleration are set to the initial ones.
	 * 			| else {
	 * 			| 	setHorizontalAcceleration(getInitialHorizontalAcceleration())
	 * 			|	setHorizontalVelocity(getInitialHorizontalVelocity())
	 * 			| }
	 */
	public void endMove() {
		if (! isMovingLeft() && ! isMovingRight()) {
			this.setHorizontalAcceleration(0);
			this.setHorizontalVelocity(0);
		}
		
		else if ( (isMovingLeft() && isPressingLeft() || (isMovingRight() && isPressingRight())) && ! hasSwitchedDirection() ) {
			this.setHorizontalAcceleration(getCurrentHorizontalAcceleration());
			this.setHorizontalVelocity(getCurrentHorizontalVelocity());
		}
		
		else {
			this.setHorizontalAcceleration(getInitialHorizontalAcceleration());
			this.setHorizontalVelocity(getInitialHorizontalVelocity());
		}
		
		
	}



	/**
	 * Mazub starts ducking.
	 * 
	 * @effect	The maximum horizontal velocity is set to the horizontal ducking velocity.
	 * 			| setMaximumHorizontalVelocity(getHorziontalDuckingVelocity())
	 * @effect	The horizontal acceleration is reset.
	 * 			| setHorizontalAcceleration(0)
	 * @effect	If Mazub is moving horizontally, his horizontal velocity is set to the
	 * 			horizontal ducking velocity.
	 * 			| if (isMovingLeft() || isMovingRight())
	 * 			| 	setHorizontalVelocity(getHorizontalDuckingVelocity())
	 */
	public void startDuck(){
		
		this.setMaximumHorizontalVelocity(getHorizontalDuckingVelocity());
		this.setHorizontalAcceleration(0);  
		
		if (isMovingLeft() || isMovingRight())
			this.setHorizontalVelocity(getHorizontalDuckingVelocity());
			
	}



	/**
	 * Mazub stops ducking.
	 * 
	 * @effect	The maximum horizontal velocity is reset to the inital maximum horizontal velocity.
	 * 			| setMaximumHorizontalVelocity(getInitialMaximumHorizontalVelocity())
	 * @effect	If Mazub is still moving horizontally, his horizontal acceleration is set to the 
	 * 			initial horizontal acceleration, else it is reset.
	 * 			| if (isMovingLeft() || isMovingRight())
	 *			|	setHorizontalAcceleration(getInitialHorizontalAcceleration())
	 * 			| else
	 * 			| 	setHorizontalAcceleration(0)
	 */
	public void endDuck(){
		this.setMaximumHorizontalVelocity(getInitialMaximumHorizontalVelocity());
		if (isMovingLeft() || isMovingRight())
			this.setHorizontalAcceleration(getInitialHorizontalAcceleration());
		else
			this.setHorizontalVelocity(0);
		setCheckingGetUp(true);
		if (getWorld().collisionTop(0, this)) {
			setDucking(true);
			startDuck();
		}
		setCheckingGetUp(false);
	}



	/**
	 * Update the position and velocity of Mazub based on the current position,
	 * velocity, acceleration and a given time duration in seconds.
	 * 
	 * @effect	Applies collision effects with the environment. 
	 * 			| applyPassableAreaCollisions(dt)
	 * @effect  Controls the immunity of Mazub.
	 * 			| checkImmunity(dt)
	 * @effect	If Mazub is falling and has a bottom collision, the vertical velocity is set to zero.
	 * 			| if (getWorld().collisionBottom(splitDt, this) && getCurrentVerticalVelocity() < 0)
	 * 			|	setVerticalVelocity(0)
	 * @effect	If Mazub is jumping and bumps his head, his vertical velocity is set to zero.
	 * 			| else if (getWorld().collisionTop(splitDt, this) && getCurrentVerticalVelocity()>0) 
	 * 			|	setVerticalVelocity(0)
	 * @effect	If none of the previous two conditions are true, the vertical velocity and position are adjusted according to the time.
	 * 			| else
	 * 			|	setVerticalVelocity(newVerticalVelocity(splitDt))
	 * 			|	setCurrentVerticalPosition(newVerticalPosition(splitDt))
	 * @effect	If Mazub is moving horizontally and has no collision in this direction, his position and velocity are adjusted according to the time.
	 * 			| if ((!getWorld().collisionRight(splitDt, this) && isMovingRight()) || (!getWorld().collisionLeft(splitDt, this) && isMovingLeft()))
	 * 			|	setCurrentHorizontalPosition(newHorizontalPosition(splitDt))
	 * 			|	setHorizontalVelocity(newHorizontalVelocity(splitDt))
	 * @effect	If Mazub has reached the target tile, the game stops and the player wins.
	 * 			| if (isTargetTile())
	 * 			|	getWorld().setGameOver(true)
	 * 			|	getWorld().setPlayerWin(true)
	 * @effect	When Mazub reaches his maximum horizontal velocity, his acceleration shall be set to zero.
	 * 			| if (Util.fuzzyGreaterThanOrEqualTo(getCurrentHorizontalVelocity(), getCurrentMaximumHorizontalVelocity()))
	 * 			| 	this.setHorizontalAcceleration(0)
	 * @effect	The time for the walking cycle index goes up with splitDt.
	 * 			| setDeltaT2(getDeltaT2() + splitDt)
	 * @effect	If the time for the walking cycle is larger than 0.075, the sprite index goes up with one and the time loses 0.075 seconds.
	 * 			| if (Util.fuzzyGreaterThanOrEqualTo(getDeltaT2(), 0.075))
	 * 			|	setWalkingCycleIndex(getWalkingCycleIndex() + 1)
	 * 			|	setDeltaT2(getDeltaT2() - 0.075)
	 * @effect	If Mazub is ducking but the ducking button isn't pressed anymore, he tries to get up.
	 * 			| if (!this.isPressingDown() && isDucking())
	 * 			| 	setDucking(false)
	 * 			|	endDuck()
	 * @effect	If Mazub is not moving, the passed time goes up with dt so that eventually he turns to face the screen again.
	 * 			| if (! isMovingLeft() && ! isMovingRight() && ! isJumping())
	 * 			| 	setPassedTime(getPassedTime() + dt)
	 */
	@Override
	public void advanceTime(double dt) throws IllegalStateException {
		applyPassableAreaCollisions(dt);
		checkImmunity(dt);
		double splitDt = splitDt(dt);
		double smallDt = splitDt;
		while(Util.fuzzyLessThanOrEqualTo(smallDt, dt) || (Util.fuzzyGreaterThanOrEqualTo(smallDt, dt) && Util.fuzzyLessThanOrEqualTo((smallDt - splitDt), dt))){
			
			if ((Util.fuzzyGreaterThanOrEqualTo(smallDt, dt) && Util.fuzzyLessThanOrEqualTo((smallDt - splitDt), dt)))
				splitDt = dt - (smallDt - splitDt);
			
			if (getWorld().collisionBottom(splitDt, this) && getCurrentVerticalVelocity() < 0){ 
				setVerticalVelocity(0);
			}
			else if (getWorld().collisionTop(splitDt, this) && getCurrentVerticalVelocity()>0){
				setVerticalVelocity(0);
			}
			else{
				setVerticalVelocity(newVerticalVelocity(splitDt));
				setCurrentVerticalPosition(newVerticalPosition(splitDt));
			}
			
			if ((!getWorld().collisionRight(splitDt, this) && isMovingRight()) || (!getWorld().collisionLeft(splitDt, this) && isMovingLeft())){
				setCurrentHorizontalPosition(newHorizontalPosition(splitDt));
				setHorizontalVelocity(newHorizontalVelocity(splitDt));
			}
			
			if (isTargetTile()){
				getWorld().setGameOver(true);
				getWorld().setPlayerWin(true);
			}
			
			if (Util.fuzzyGreaterThanOrEqualTo(getCurrentHorizontalVelocity(), getCurrentMaximumHorizontalVelocity()))
				this.setHorizontalAcceleration(0);
			
			if (!this.isPressingDown() && isDucking()) {
				setDucking(false);
				endDuck();
			}
			
		smallDt += splitDt;
		}
		
		setDeltaT2(getDeltaT2() + dt);
		
		if (Util.fuzzyGreaterThanOrEqualTo(getDeltaT2(), 0.075)) {
				setWalkingCycleIndex(getWalkingCycleIndex() + 1);
				setDeltaT2(getDeltaT2() - 0.075);
		}
		
		if (! isMovingLeft() && ! isMovingRight() && ! isJumping())
			setPassedTime(getPassedTime() + dt);
	}



	/**
	 * Sets the ducking state of this Mazub according
	 * to the given value.
	 * 
	 * @param 	value
	 * 			The new ducking state of this Mazub.
	 * @post	The new ducking state of this Mazub is 
	 * 			equal to the given value.
	 * 			| new.isDucking == value
	 */
	public void setDucking(boolean value) {
		this.isDucking = value;
	}



	/**
	 * Sets if the left arrow is pressed to a given flag.
	 * @param flag
	 * 		Indicates the new state of the left arrow.
	 * @post isPressingLeft is set to the new flag.
	 * 		| new.isPressingLeft() == flag
	 */
	public void setPressingLeft(boolean flag) {
		this.isPressingLeft = flag;
	}



	/**
	 * Sets if the right arrow is pressed to a given flag.
	 * @param flag
	 * 		Indicates the new state of the right arrow.
	 * @post isPressingRight is set to the new flag.
	 * 		| new.isPressingRight() == flag
	 */
	public void setPressingRight(boolean flag) {
		this.isPressingRight = flag;
	}



	/**
	 * Sets if the arrow down is pressed to a given flag.
	 * @param flag
	 * 		Indicates the new state of the arrow down.
	 * @post isPressingDown is set to the new flag.
	 * 		| new.isPressingDown() == flag
	 */
	public void setPressingDown(boolean flag) {
		this.isPressingDown = flag;
	}

	/**
	 * Returns if Mazub is allowed to jump.
	 * @return True if Mazub has a collision below him, so that he is able to jump false if there is none.
	 * 			| result == getWorld().collisionBottom(0, this)
	 */
	protected boolean isAllowedToJump() {
		return this.getWorld().collisionBottom(0, this);		
	}



	/**
	* Check whether the game object is ducking.
	*/
	@Basic
	protected boolean isDucking() {
		return this.isDucking;
	}



	/**
	 * Returns if the method endDuck() is checking if Mazub can get up.
	 */
	private boolean isCheckingGetUp() {
		return this.isCheckingGetUp;
	}



	/**
	 * Sets the given flag to indicate if endMove() checks if Mazub can get up.
	 * @param flag
	 * 		The flag to indicate if endDuck() is checking if Mazub can get up.
	 * @post isCheckingGetUp() is set to a new flag.
	 * 		|new.isCheckingGetUp() == flag
	 */
	private void setCheckingGetUp(boolean flag) {
		this.isCheckingGetUp = flag;
	}

	private boolean hasSwitchedDirection;
	
	private boolean isPressingLeft;

	
	private boolean isPressingRight;
	
	private boolean isPressingDown;
	
	
	private boolean isCheckingGetUp;
	
	private boolean isDucking;
	private boolean lastMoveWasRight;



	
	
}

