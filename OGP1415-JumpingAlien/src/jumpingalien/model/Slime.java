package jumpingalien.model;

import java.util.List;
import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

/**
 * A class of Slimes as a kind of GameObject. 
 * 
 * @version 1.0
 * @author Bram Vandendriessche 
 * @author Maaike Van Roy
 */
public class Slime extends GameObject{

	/**
	 * Initialize this new Slime with the given horizontal position, 
	 * given vertical position, given array of sprite images and given school.
	 * 
	 * @param 	x
	 * 		  	The horizontal position for this new Slime.
	 * @param 	y
	 * 			The vertical position for this new Slime.
	 * @param 	sprites
	 * 			The array of sprite images.
	 * @param 	school
	 * 			The school of this new Slime.
	 * @effect	| super(x,y,sprites,100,2.5,1,0,0.7,-10,100,0.6)
	 * @effect	| this.setSchool(school)
	 * @effect  | this.chooseDirection()
	 * @effect	| this.generateMovementDuration()
	 * @effect	| this.startMove()
	 */
	public Slime(int x, int y, Sprite[] sprites, School school) {
		super(x, y, sprites, 100, 2.5, 1, 0, .7, -10, 100, 0.6);
		this.setSchool(school);
		this.chooseDirection();
		this.generateMovementDuration();
		this.startMove();
		
	}
	
	// schools
	
	/**
	 * Returns the school of this Slime.
	 */
	@Basic
	public School getSchool() {
		return school;
	}

	/**
	 * Sets the school of this Slime to the given school and 
	 * adds this Slime to the school.
	 * 
	 * @param 	school
	 * 			The new school of this slime.
	 * @post	| new.getSchool() == school
	 * @effect	| getSchool().addSlime(this)
	 */
	private void setSchool(School school) {
		this.school = school;
		school.addSlime(this);
	}
	
	private School school;
	
	// movement
	
		
	/**
	 * Returns true or false if this Slime is allowed to jump.
	 * 
	 * @return	 false
	 */
	@Override
	protected boolean isAllowedToJump() {
		return false;
	}
	
	/**
	 * Update the immunity, position, direction and velocity of this Slime based on
	 * the given time duration, current immunity, position, direction and velocity.
	 * 
	 * @effect	| applyPassableAreaCollisions(dt)
	 * @effect	| checkImmunity(dt)
	 * @effect	| if (Util.fuzzyGreaterThanOrEqualTo(this.getLocalTime() + dt, this.getMovementDuration()))
	 * 			|	this.setLocalTime(0)
	 * 			|	if (this.getMovementDuration() - this.getLocalTime() > 0)
	 * 			|		this.changeLocation(this.getMovementDuration() - this.getLocalTime())
	 * 			|	generateMovementDuration()
	 * 			|	this.chooseDirection()
	 * 			|	this.changeLocation(this.getLocalTime() - this.getMovementDuration())
	 * @effect	| else
	 * 			|	changeLocation(dt)
	 */
	@Override
	protected void advanceTime(double dt) {
		applyPassableAreaCollisions(dt);
		checkImmunity(dt);
		double splitDt = splitDt(dt);
		double smallDt = splitDt;
		while(Util.fuzzyLessThanOrEqualTo(smallDt, dt)){
			setLocalTime(this.getLocalTime() + splitDt);
			if (Util.fuzzyGreaterThanOrEqualTo(this.getLocalTime(), this.getMovementDuration())) {
		
				double oldTime = this.getLocalTime();
				double oldDuration = this.getMovementDuration();
				this.setLocalTime(0);
				if (this.getMovementDuration() - oldTime > 0)
					this.changeLocation(this.getMovementDuration() - oldTime);
				generateMovementDuration();
				this.chooseDirection();
				this.changeLocation(oldTime - oldDuration);
				
			}
			
			else {
				changeLocation(splitDt);
				
			}
		smallDt += splitDt;	
		}
		
	}
	
	/**
	 * Returns the length of a specific movement.
	 */
	private double getMovementDuration() {
		return this.movementDuration;
	}
	
	/**
	 * Returns the local time of this Slime.
	 */
	private double getLocalTime() {
		return localTime;
	}
	
	/**
	 * Randomly chooses a movement direction and sets the horizontal velocity
	 * to the initial horizontal velocity.
	 * 
	 * @effect	| if ((new Random()).nextBoolean())
	 * 			|	setMovingRight(true)
	 * 			|	setMovingLeft(false)
	 * 			| else
	 * 			|	setMovingRight(false)
	 * 			|   setMovingLeft(true)
	 * @post	| new.getCurrentHorizontalVelocity() == getInitialHorizontalVelocity()
	 * 
	 */
	private void chooseDirection() {
		if ((new Random()).nextBoolean()) {
			setMovingRight(true);
			setMovingLeft(false);
		}
		else{
			setMovingRight(false);
			setMovingLeft(true);
		}
		this.setHorizontalVelocity(getInitialHorizontalVelocity());
			
	}
	
	/**
	 * Changes the location and velocity of this Slime according to his movement direction,
	 * velocity, position, and the given time duration.
	 * 
	 * @param 	dt
	 * 			The given time duration.
	 * @effect	| if (getWorld() != null && !isTerminated())
	 * 			| 	setHorizontalVelocity(newHorizontalVelocity(dt))
	 * @effect	|if (getWorld() != null && !isTerminated())
	 * 			| 	setHorizontalVelocity(newHorizontalVelocity(dt))
	 * 			|	if (isMovingLeft() && !(getWorld().collisionLeft(dt, this)))
	 * 			|		setCurrentHorizontalPosition(getCurrentHorizontalPosition() - getHorizontalDistanceTravelled(dt))	
	 * @effect  |if (getWorld() != null && !isTerminated())
	 * 			| 	setHorizontalVelocity(newHorizontalVelocity(dt))
	 * 			|	else if (isMovingRight() && !(getWorld().collisionRight(dt, this)))
	 *			|		setCurrentHorizontalPosition(getCurrentHorizontalPosition() + getHorizontalDistanceTravelled(dt))
	 * @effect  |if (getWorld().collisionBottom(dt, this))
	 * 			|	setVerticalVelocity(0)
	 *			|   setVerticalAcceleration(0)
	 *			|else
	 *			|	setCurrentVerticalPosition(newVerticalPosition(dt))
	 *			|	setVerticalVelocity(newVerticalVelocity(dt))
	 */
	private void changeLocation(double dt) {
		if (getWorld() != null && !isTerminated()) {
			setHorizontalVelocity(newHorizontalVelocity(dt));
			if (isMovingLeft() && !(getWorld().collisionLeft(dt, this)))
				setCurrentHorizontalPosition(getCurrentHorizontalPosition() - getHorizontalDistanceTravelled(dt));
			else if (isMovingRight() && !(getWorld().collisionRight(dt, this)))
				setCurrentHorizontalPosition(getCurrentHorizontalPosition() + getHorizontalDistanceTravelled(dt));
			if(getWorld().collisionBottom(dt, this)) {
				setVerticalVelocity(0);
				setVerticalAcceleration(0);
			}
			else {
				setCurrentVerticalPosition(newVerticalPosition(dt));
				setVerticalVelocity(newVerticalVelocity(dt));
			}
		}

	}
	

	/**
	 * Generates a random length for a movement.
	 * 
	 * @post	| new.getMovementDuration() == 2 + (4) * random.nextDouble()
	 */
	private void generateMovementDuration() {
		Random random = new Random();
		this.movementDuration =  2 + (4) * random.nextDouble();
	}
	
	/**
	 * Sets the local time of this Slime to the given time.
	 * 
	 * @param 	localTime
	 * 			The given time.
	 * @post	| new.getLocalTime() == localTime
	 */
	private void setLocalTime(double localTime) {
		this.localTime = localTime;
	}
	
	private double localTime;
	private double movementDuration;

	// sprites
	
	/**
	 * Check whether the given array is a valid array.
	 * 
	 * @return	False if the length of the array doesn't equal 2.
	 * 			| result == (array.length != 2)
	 * @return	True if and only if the length of the array is equal to 2.
	 * 			| result == (array.length == 2)
	 */
	
	@Override	
	protected boolean isValidArray(Sprite[] array){
		return (array.length == 2);
	}
	
	
	
	/**
	 * Returns the sprite image that is equivalent with the Slime's movement.
	 * 
	 * @return	| if (isMovingLeft())
	 * 			|	return getSprite(0)
	 * 			| else
	 * 			|	return getSprite(1)
	 */
	public Sprite getCurrentSprite() {
		if (isMovingLeft())
			return getSprite(0);
		else
			return getSprite(1);
	}
	
	
	
	// collision
	
	/**
	 * Returns the time a Slime is positioned on a specific feature.
	 */
	private double getFeatureTime() {
		return this.featureTime;
	}

	/**
	 * Applies the effects of a collision to this Slime and it's collisioner.
	 * 
	 * @effect	| if (object is Shark)
	 * 			|	if (!this.isImmune())
	 * 			|		setNbHitPoints(getNbHitPoints() - 50);
	 *			|		getSchool().collisionEffectOtherSlimes(this);
	 *			|		setImmunity(true);
	 * @effect	| if (object is Shark)
	 * 			|	if (!object.isImmune())
	 * 			|		object.setNbHitPoints(object.getNbHitPoints() - 50);
	 *			|		object.setImmunity(true);
	 * @effect  | if (object is Slime)
	 * 			|	if (object.getSchool() != this.getSchool())
	 * 			|		if (object.getSchool().schoolSize() > this.getSchool().schoolSize())
	 * 			|			getSchool().changeOldSchoolEffect(this)
	 * 			|			object.getSchool().changeNewSchoolEffect(this)
	 * 			|			this.setSchool(object.getSchool())
	 * @effect	| if (object is Slime)
	 * 			|	if (object.getSchool() != this.getSchool())
	 * 			|  		if (this.getSchool().schoolSize() > object.getSchool().schoolSize()
	 * 			|			object.getSchool().changeOldSchoolEffect(object)
	 * 			|			getSchool().changeNewSchoolEffect(object)
	 * 			|			object.setSchool(this.getSchool())
	 */
	@Override
	protected void applyCollisionEffects(GameObject object, boolean bottom) {
		if (object instanceof Shark){
			if (!this.isImmune()){
				setNbHitPoints(getNbHitPoints() - 50);
				getSchool().collisionEffectOtherSlimes(this);
				setImmunity(true);
			}
			if (!object.isImmune()){
				object.setNbHitPoints(object.getNbHitPoints() - 50);
				object.setImmunity(true);
			}
						
		}
		
		 if (object instanceof Slime){
		 	 if (((Slime) object).getSchool() != this.getSchool()){
		 		if (((Slime) object).getSchool().schoolSize() > this.getSchool().schoolSize()){
		 			getSchool().changeOldSchoolEffect(this);
			 		((Slime) object).getSchool().changeNewSchoolEffect(this);
		 			this.setSchool(((Slime) object).getSchool());
		 		}
		 		else if (this.getSchool().schoolSize() > ((Slime) object).getSchool().schoolSize()){
		 			((Slime) object).getSchool().changeOldSchoolEffect((Slime)object);
		 			getSchool().changeNewSchoolEffect((Slime) object);
		 			((Slime) object).setSchool(this.getSchool());
		 		}
		 	 }
		 }
	}
	
	

	/**
	 * Applies the effects of a passable area collision for this Slime.
	 * 
	 * @effect	| if (getFeatureTime() == -1)
	 * 			| 	for each feat in featureList
	 * 			|		if (feat == 3)
	 * 			|			setFeatureTime(dt)
	 * 			|			setNbHitPoints(getNbHitPoints() - 50)
	 * @effect	| if (getFeatureTime() == -1)
	 * 			| 	for each feat in featureList
	 * 			|		else if (feat == 2)
	 * 			|			setNbHitPoints(getNbHitPoints() - 2);
	 *          |			setFeatureTime(getFeatureTime() - .2 + dt);
	 * @effect	| else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2))
	 * 			|	for each feat in featureList
	 * 			|		if (feat == 2)
	 * 			|			setNbHitPoints(getNbHitPoints() - 2);
	 *		    |		    setFeatureTime(getFeatureTime() - .2 + dt);
	 * @effect	| else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2))
	 * 			|	for each feat in featureList
	 * 			|		else if (feat == 3)
	 * 			|           setNbHitPoints(getNbHitPoints() - 50);
	 *    		|	        setFeatureTime(getFeatureTime() - .2 + dt);
	 * @effect  | else
	 * 			|	setFeatureTime(getFeatureTime() + dt);
	 * @effect	| if (noDamage == true) 
	 *			|   setFeatureTime(-1)
	 * @post	| if (getFeatureTime() == -1)
	 * 			| 	for each feat in featureList
	 * 			|		if (feat == 3)
	 * 			|			new.noDamage == false
	 * @post    | if (getFeatureTime() == -1)
	 * 			| 	for each feat in featureList
	 * 			|		else if (feat == 2)
	 * 			|			new.noDamage == false
	 * @post    | else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2))
	 * 			|	for each feat in featureList
	 * 			|		if (feat == 2)
	 * 			|			new.noDamage == false
	 * @post	| else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2))
	 * 			|	for each feat in featureList
	 * 			|		else if (feat == 3)
	 * 			|			new.noDamage == false
	 * @post	| else
	 * 			| 	new.noDamage == false
	 */
	@Override
	protected void applyPassableAreaCollisions(double dt) {
	  List<Integer> featureList = this.getWorld().passableAreaCollisionList(this);
	  //3 is magma
	  //2 is water
	  //1 is solid
	  //0 is lucht
	  boolean noDamage = true;
	  if (getFeatureTime() == -1) {
	    for (Integer feat: featureList) {
	      if (feat == 3) {
	        setFeatureTime(dt);
	        setNbHitPoints(getNbHitPoints() - 50);
	        noDamage = false;
	        break;
	      }
	      else if (feat == 2) {
	        setFeatureTime(dt);
	        noDamage = false;
	        break;
	      }
	    }
	  }
	  
	  else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2)) {

	    for (Integer feat: featureList) {
	      if (feat == 2) {
	        setNbHitPoints(getNbHitPoints() - 2);
	        setFeatureTime(getFeatureTime() - .2 + dt);
	        noDamage = false;
	        break;
	      }
	      else if (feat == 3) {
	        setNbHitPoints(getNbHitPoints() - 50);
	        setFeatureTime(getFeatureTime() - .2 + dt);
	        noDamage = false;
	        break;
	      }
	    }
	  }
	  else {
	    setFeatureTime(getFeatureTime() + dt);
	    noDamage = false;

	  }
	  
	  if (noDamage == true) { 
	    setFeatureTime(-1);
	  }
	}
	

	/**
	 * Sets the feature time to the given time.
	 * 
	 * @param 	time
	 * 			The given time.
	 * @post	new.getFeatureTime() == time
	 */
	private void setFeatureTime(double time) {
		this.featureTime = time;
	}
	
	private double featureTime;

}
