package jumpingalien.model;

import jumpingalien.util.Sprite;
import jumpingalien.util.Util;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class of game objects with specific properties like a horizontal and vertical position, velocity and acceleration, an amount of hitpoints etc.
 * 
 * @invar	The position of the game object must at all times be a valid position.
 * 			| isValidHorizontalPosition(getCurrentHorizontalPosition) &&
 * 			| 	isValidVerticalPosition(getCurrentVerticalPosition)
 * @invar	The velocity of the game object must at all times be a valid velocity.
 * 			| isValidHorizontalVelocity(getCurrentHorizontalVelocity) &&
 * 			| 	isValidVerticalVelocity(getCurrentVerticalVelocity)
 * @invar	The array of sprite images must at all times be a valid array.
 * 			| isValidArray(sprites)
 * @invar	The amount of hitpoints the game object possesses must at all times be a valid amount
 * 			| isValidAmountOfHitpoints(getNbHitpoints)
 * 
 * @author Bram Vandendriessche
 * @author Maaike Van Roy
 */
public abstract class GameObject {
	
	/**
	 * Initialize the new game object with a given horizontal and vertical position, velocity, acceleration, a maximum and initial amount of hitpoints, a time to live and a list of sprite images.
	 * @param xPos
	 * 		The initial horizontal position of the game object.
	 * @param yPos
	 * 		The initial vertical position of the game object.
	 * @param sprites
	 * 		The array of sprite images for the game object.
	 * @param hitPoints
	 * 		The initial amount of hitpoints of the game object.
	 * @param maximumHorizontalVelocity
	 * 		The maximum horizontal velocity the game object can have.
	 * @param initialXVelocity
	 * 		The velocity of the game object at the start of a horizontal movement.
	 * @param initialYVelocity
	 * 		The velocity of the game object at the start of a vertical movement.
	 * @param initialXAcceleration
	 * 		The acceleration of the game object at the start of a horizontal movement.
	 * @param initialFallAcceleration
	 * 		The acceleration of the game object when it moves vertically.
	 * @param maxHitpoints
	 * 		The maximum amount of hitpoints the game object can get.
	 * @param timeToLive
	 * 		The time in seconds before a game object dies after having its hitpoints set to zero.
	 * @effect
	 * 		| getNbOfHitPoints() == hitpoints
	 * @effect
	 * 		| getCurrentHorizontalPosition() == xPos
	 * @effect
	 * 		| getCurrentVerticalPosition() == yPos
	 * @effect
	 * 		| getTimeToLive() == timeToLive
	 * @post 
	 * 		| new.sprites = sprites
	 * @post
	 * 		| new.getInitialMaximumHorizontalVelocity() == maximumHorizontalVelocity
	 * @post
	 * 		| new.getInitialHorizontalVelocity == initialXVelocity
	 * @post
	 * 		| new.getInitialVerticalVelocity== initialYVelocity
	 * @post
	 * 		| new.getInitialFallingAcceleration() == initialFallAcceleration
	 * @post
	 * 		| new.getInitialHorizontalAcceleration() == initialXAcceleration
	 * 
	 */
	protected GameObject(
			int xPos, int yPos, Sprite sprites[], int hitPoints, 
			double maximumHorizontalVelocity, double initialXVelocity, 
			double initialYVelocity, double initialXAcceleration, 
			double initialFallAcceleration, int maxHitpoints, double timeToLive) {
		this.sprites = sprites;
		this.maxAmountOfHitpoints = maxHitpoints;
		this.setNbHitPoints(hitPoints);
		this.setCurrentHorizontalPosition(xPos);
		this.setCurrentVerticalPosition(yPos);
		this.initialMaximumHorizontalVelocity = maximumHorizontalVelocity;
		this.initialHorizontalVelocity = initialXVelocity;	
		this.initialVerticalVelocity = initialYVelocity; //als ge setters hebt die ook gebruiken! alleen finals op deze manier initialiseren
		this.initialFallingAcceleration = initialFallAcceleration;
		this.initialHorizontalAcceleration = initialXAcceleration;
		this.setTimeToLive(timeToLive);

	}
	
	//  ########## HITPOINTS
	
	/**
	 * Returns the amount of hitpoins that the game object possesses.
	 */
	@Basic
	public int getNbHitPoints(){
		return this.nbHitPoints;
	}
	
	/**
	 * Returns if the given amount is a valid amount for hitpoints.
	 * @param amount
	 * 		The amount that has to be validated.
	 * @return
	 * 		| result == (amount > 0 && amount <= getMaxAmountOfHitpoints())
	 */
	protected boolean isValidAmountOfHitpoints(int amount) {
		return (amount > 0 && amount <= getMaxAmountOfHitpoints());
	}
	
	/**
	 * Returns the maximal amount of hitpoints that the game object can get.
	 */
	@Basic
	protected int getMaxAmountOfHitpoints() {
		return this.maxAmountOfHitpoints;
	}
	
	/**
	 * Set the hitpoints of the game object to a given amount.
	 * @param amount
	 * 		The amount of hitpoints that the game object should get.
	 * @post
	 * 		| if(isValidAmountOfHitPoinst(amount) {
	 * 		| 		new.getNbHitPoints() == amount
	 * 		| }
	 * @post
	 * 		| else if(amount <= 0) {
	 * 		|		new.getNbHitPoints() == 0
	 * 		| 		new.isTerminated() == true
	 * 		| }
	 */
	protected void setNbHitPoints(int amount){
		if (isValidAmountOfHitpoints(amount)){
			this.nbHitPoints = amount;
		}
		else if (amount <= 0){
			this.nbHitPoints = 0;
			this.setTerminated(true);		
	    }
	 }

	protected int nbHitPoints;
	private int maxAmountOfHitpoints;
	
	
	// ############# IMMUNITY
	
	/**
	 * Returns if the object is immune.
	 */
	@Basic
	public boolean isImmune(){
		return isImmune;
	}

	/**
	 * Controls the immunity of the game object.
	 * 
	 * @param dt
	 * 		The amount of time that should be added to the current time of immunity if this doesn't pass the limit of .6 seconds.
	 * @effect
	 * 		| if(this.isImmune() && Util.fuzzyLessThanOrEqualTo(getImmuneTime(), 0.6)) 
	 * 		|		addTimeToImmune(dt)
	 * 		| else if(this.isImmune() && Util.fuzzyGreaterThanOrEqualTo(getImmuneTime(), 0.6))
	 * 		| 		setImmunity(false)
	 * 		|		resetImmuneTime()
	 */
	protected void checkImmunity(double dt){
		if (this.isImmune() && Util.fuzzyLessThanOrEqualTo(getImmuneTime(), 0.6)){
			addTimeToImmune(dt);
		}
		else if (this.isImmune() && Util.fuzzyGreaterThanOrEqualTo(getImmuneTime(), 0.6)){
			setImmunity(false);
			resetImmuneTime();
		}
	}
	
	/**
	 * Sets if the object is immune or not
	 * 
	 * @param flag
	 * 		The boolean that must be given to immune.
	 * @post
	 * 		| new.getImmunity() = flag
	 */
	protected void setImmunity(boolean flag){
		this.isImmune = flag;
	}
	
	protected boolean isImmune;
	

	/**
	 * Returns the immune time of the object.
	 */
	@Basic
	private double getImmuneTime() {
		return this.immuneTime;
	}
	

	/**
	 * Add a given amount of time to the immuneTime
	 * @param amount
	 * 		The amount of time that has to be added to the immuneTime.
	 * @post
	 * 		| new.getImmuneTime() == old.getImmuneTime() + amount
	 */
	private void addTimeToImmune(double amount) {
		this.immuneTime += amount;
	}
	
	/**
	 * Reset the immuneTime to zero.
	 * @post
	 * 		| new.getImmuneTime() == 0
	 */
	private void resetImmuneTime() {
		this.immuneTime = 0;
	}
	
	private double immuneTime;
	
	// ########### DEATH
	
	/**
	 * Return the time to live.
	 */
	protected double getTimeToLive(){
		return timeToLive;
	}
	
	/**
	  * Returns if the game object is terminated.
	  */
	 protected boolean isTerminated(){
		 return this.isTerminated;
	 }
	
	/**
	 * Set the time to live to a given value.
	 * @param time
	 * 		The value to which timeToLive should be set.
	 * @post
	 * 		|new.getTimeToLive() == time
	 */
	protected void setTimeToLive(double time){
		this.timeToLive = time;
	}
	
	/**
	  * Set if the game object is terminated to a given flag.
	  * @param flag
	  * 	The given value to which the terminated state of the game object should be set.
	  * @post
	  * 	| new.isTerminated() == flag
	  */
	 protected void setTerminated(boolean flag){
		 this.isTerminated = flag;
	 }
	
	 private boolean isTerminated;
	 private double timeToLive;
	
	//  ###############  COLLISION
	
	/**
	 * Applies the effects of collisions of this game object with eventual others.
	 * @param object
	 * 		The other colliding game object
	 * @param bottom
	 * 		Boolean telling if the game object is colliding the other object with its bottom.
	 */
	protected abstract void applyCollisionEffects(GameObject object, boolean bottom);
	
	
	//   ###############  WORLD 
	
	/**
	 * Apply the effects of collisions between the game object and its passable environment.
	 * @param dt
	 * 		The amount of time with which the local time is advanced to check if the object is (still) being damaged by its environments.
	 */
	protected abstract void applyPassableAreaCollisions(double dt);

	/**
	 * Returns the world of the game object.
	 */
	@Basic
	protected World getWorld() {
		return this.world;
	}
	
	/**
	 * Set the world of the game object to a given world.
	 * @param world
	 * 		The world to which the game object's world should be set.
	 * @post
	 * 		| new.getWorld() = world
	 */
	protected void setWorld(World world){
		this.world = world;
	}

	private World world;
	
	
	//		################  POSITION
	
	/**
	* Return the current vertical position of the game object.
	*/
	@Basic 
	public double getCurrentVerticalPosition() {
		return this.currentVerticalPosition;
	}
	
	/**
	* Return the current horizontal position of the game object.
	*/
	@Basic 
	public double getCurrentHorizontalPosition() {
		return this.currentHorizontalPosition;
	}	
	
	
	/**
	* Return the maximum vertical position that the game object can reach.
	* 
	* @return 	The highest possible value for the game object's vertical position is not
	* 			below the lowest possible value for the vertical position of this
	* 			the game object.
	* 			| result >= getMinimumVerticalPosition()
	*/
	@Basic @Immutable
	protected double getMaximumVerticalPosition(){
		return this.getWorld().getGameWorldHeigth() - 1;
	}
	
	
	/**
	* Check whether a given horizontal position is valid.
	* 
	* @param 	horizontalPosition
	* 			The position that has to be checked.
	* @return	True if the game world is null.
	* 			| result = (getWorld() == null)
	* @return 	False if the position is smaller than the minimum horizontal position 
	* 			or larger than the maximum horizontal position.
	* 			| result == !((horizontalPosition < getMinimumHorizontalPosition()) ||
	* 				(horizontalPosition > getMaximumHorizontalPosition()))
	* @return 	True if and only if the horizontal position lies between the minimum 
	* 			and maximum horizontal position.
	* 			| result == ((horizontalPosition >= getMinimumHorizontalPosition()) &&
	* 				(horizontalPosition <= getMaximumHorizontalPosition()))
	*/
	protected  boolean isValidHorizontalPosition(double horizontalPosition){
		if (getWorld() == null){
			return true;
		}
		if (horizontalPosition >= getMaximumHorizontalPosition()){
			return false;
		}
		if (horizontalPosition < getMinimumHorizontalPosition()){
			return false;
		}
		else
			return true;
	}
	
	/**
	* Check whether a given vertical position is valid.
	* 
	* @param 	verticalPosition
	* 			The position that has to be checked.
	* @return	True if the game world is null.
	* 			| result == (getWorld() == null)
	* @return	False if the position is lower than the minimum vertical position
	* 			or higher than the maximal vertical position.
	* 			| result == !(verticalPosition < getMinimumVerticalPosition() || 
	* 			| verticalPosition > getMaximumVerticalPosition())
	* @return	True if and only if the vertical position lies between the minimal 
	* 			and maximal vertical position.
	* 			| result == (verticalPosition >= getMinimumVerticalPosition() && 
	* 			| verticalPosition <= getMaximumVerticalPosition())
	*/
	protected boolean isValidVerticalPosition(double verticalPosition) {
		if (getWorld() == null){
			return true;
		}
		if (verticalPosition < getMinimumVerticalPosition())
			return false;
		if (verticalPosition >= getMaximumVerticalPosition())
			return false;
		else
			return true;
	}
	
	/**
	* Returns the horizontal distance the game object traveled while moving.
	* 
	* @param 	dt
	* 			The given time duration in seconds.
	* @return	The horizontal distance traveled is equal to the result
	* 			of the formula.
	*/
	protected double getHorizontalDistanceTravelled(double dt){
		double distanceTravelled;
		distanceTravelled = 100 * (getCurrentHorizontalVelocity() * dt + (getCurrentHorizontalAcceleration()*Math.pow(dt, 2))/2); 
		return distanceTravelled;
	}
	
	
	/**
	* Sets the horizontal position of the game object to the given horizontal position.
	* 
	* @param 	position
	* 			The new horizontal position for this the game object.
	* @post
	* 			| if(!isValidHorizontalPosition(position)
	* 			| 		new.isTerminated == true
	* 			| else
	* 			| 		new.getCurrentHorizontalPosition() == position
	*/
	protected void setCurrentHorizontalPosition(double position){
		if (! isValidHorizontalPosition(position))
			this.setTerminated(true);
		else
			this.currentHorizontalPosition = position;
	}
	
	/**
	* Sets the vertical position of the game object to the given vertical position.
	* 
	* @param 	position
	* 			The new vertical position for this the game object.
	* @post
	* 			| if(!isValidVerticalPosition(position)
	* 			| 		new.isTerminated == true
	* 			| else
	* 			| 		new.getCurrentVerticalPosition() == position
	*/
	protected void setCurrentVerticalPosition(double position){
		if (! isValidVerticalPosition(position))
			this.setTerminated(true);
		else
			this.currentVerticalPosition = position;
	}
	
	/**
	* Return the new horizontal position of this the game object.
	* 
	* @param 	dt
	* 			The given time in seconds.
	* @return	The new horizontal position is equal to the result
	* 			of the formula.
	* 			|if (isMovingLeft())
	* 			|	then result == (newHorizontalPosition = getCurrentHorizontalPosition() - 
	* 			|						getDistanceTravelled(dt))
	* 			|else
	* 			| 	then result == (newHorizontalPosition = getCurrentHorizontalPosition() + 
	* 			|						getDistanceTravelled(dt))
	*/
	protected double newHorizontalPosition(double dt){
		double newHorizontalPosition;
		if (isMovingLeft())
			newHorizontalPosition = getCurrentHorizontalPosition() - getHorizontalDistanceTravelled(dt);
		else
			newHorizontalPosition = getCurrentHorizontalPosition() + getHorizontalDistanceTravelled(dt);
		return newHorizontalPosition;
	}
	
	/**
	* Return the new vertical position of this the game object.
	* 
	* @param 	dt
	* 			The given time in seconds.
	* @return  The new vertical position is equal to the result
	* 			of the formula.
	* 			|result == getCurrentVerticalPosition() + getVerticalDistanceTravelled(dt)
	**/
	protected double newVerticalPosition(double dt){
		double newVerticalPosition;
		newVerticalPosition = getCurrentVerticalPosition() + getVerticalDistanceTravelled(dt);
		return newVerticalPosition;
	}
	
	/**
	* Return the vertical distance the game object traveled while moving.
	* 
	* @param 	dt
	* 			The given time duration in seconds.
	* @return	The vertical distance traveled is equal to the result
	* 			of the formula.
	*/
	private double getVerticalDistanceTravelled(double dt){
		double distanceTravelled;
		distanceTravelled = 100*( getCurrentVerticalVelocity()*dt + (getCurrentVerticalAcceleration()*Math.pow(dt,2))/2);
		return distanceTravelled;
	}
	
	/**
	* Return the minimum horizontal position that the game object can reach.
	* 
	* @return 	The lowest possible value that the game object can have as his horizontal position
	* 			is a non-negative number
	* 			| result >= 0
	*/
	@Basic @Immutable
	private double getMinimumHorizontalPosition(){
		return this.minimumHorizontalPosition;
	}
	
	/**
	* Return the minimum vertical position that the game object can reach.
	* 
	* @return 	The lowest possible value that the game object can have as its vertical position
	* 			is a non-negative value.
	* 			| result >= 0
	*/
	@Basic @Immutable
	private double getMinimumVerticalPosition() {
		return this.minimumVerticalPosition;
	}
	
	/**
	* Return the maximum horizontal position that the game object can reach.
	* 
	* @return 	The highest possible value for the game object's horizontal position is 
	* 			not below the lowest possible value for the horizontal position of
	* 			this the game object.
	* 			| result >= getMinimumHorizontalPosition()
	*/
	@Basic @Immutable
	private double getMaximumHorizontalPosition(){
		return this.getWorld().getGameWorldWidth() - 1;
	}
	
	private double currentHorizontalPosition;
	private double currentVerticalPosition;
	private final double minimumHorizontalPosition = 0;
	private final double minimumVerticalPosition = 0;
	
	
	
	
	// 													################  VELOCITY
	
	/**
	 * Return the current vertical velocity of the game object.
	 */
	@Basic
	public double getCurrentVerticalVelocity() {
		return this.currentVerticalVelocity;
	}
	
	/**
	 * Return the current horizontal velocity of the game object.
	 */
	@Basic
	public double getCurrentHorizontalVelocity() {
		return this.currentHorizontalVelocity; 	
	}	


	
	/**
	 * Return the initial horizontal velocity, 
	 * that is the velocity that the game object gets at the beginning 
	 * of a horizontal movement.
	 * 
	 * @return	The velocity that the game object initially gets at a horizontal movement.
	 * 			| result == initialHorizontalVelocity
	 * @return 	The initial horizontal velocity will never
	 * 			be lower than 1 m/s.
	 * 			| result >= 1
	 */
	@Immutable
	protected double getInitialHorizontalVelocity(){
		return this.initialHorizontalVelocity;
	}
	
	/**
	 * Return the game object's initial vertical velocity.
	 */
	@Immutable
	protected double getInitialVerticalVelocity(){
		return this.initialVerticalVelocity;
	}
	

	
	/**
	 * Return the initial maximum horizontal velocity that the game object can reach.
	 */
	@Immutable
	protected double getInitialMaximumHorizontalVelocity() {
		return this.initialMaximumHorizontalVelocity;
	}

	/**
	 * Check whether a given vertical velocity is valid.
	 * 
	 * @param 	verticalVelocity
	 * 			The velocity that has to be checked.
	 * @return	False if the velocity is higher than the initial vertical
	 * 			velocity.
	 * 			| result == (verticalVelocity > getInitialVerticalVelocity())
	 * @return	True if and only if the velocity is lower than the initial
	 * 			vertical velocity.
	 * 			| result == (verticalVelocity <= getInitialVerticalVelocity())
	 */
	protected boolean isValidVerticalVelocity(double verticalVelocity) {
		return Util.fuzzyLessThanOrEqualTo(verticalVelocity, getInitialVerticalVelocity());
	}
	
	/**
	 * Sets the horizontal velocity of the game object to the given velocity.
	 * 
	 * @param 	velocity
	 * 			The new horizontal velocity of this the game object.
	 * @post	The new horizontal velocity of this the game object is equal to
	 * 			the given velocity.
	 * 			| new.getCurrentHorizontalVelocity() == velocity
	 * @throws 	IllegalStateException
	 * 			The given velocity is not a valid horizontal velocity.
	 * 			| ! isValidHorizontalVelocity(velocity)
	 */
	protected void setHorizontalVelocity(double velocity) {
		if (! isValidHorizontalVelocity(velocity)){
			if (velocity > getInitialMaximumHorizontalVelocity()){
				this.currentHorizontalVelocity = getInitialMaximumHorizontalVelocity();
			}
			else if (velocity < 0)
				this.currentHorizontalVelocity = 0;
		}
		else
			this.currentHorizontalVelocity = velocity;
	}
	

	
	/**
	 * Sets the vertical velocity of the game object to the given velocity.
	 * 
	 * @param 	velocity
	 * 			The new vertical velocity for this the game object.
	 * @post	The new vertical velocity of this the game object is equal to 
	 * 			the given velocity.
	 * 			| new.getCurrentVerticalVelocity() == velocity
	 * @throws 	IllegalStateException
	 * 			The given velocity is not a valid vertical velocity.
	 * 			| ! isValidVerticalVelocity(velocity)
	 */
	protected void setVerticalVelocity(double velocity) throws IllegalStateException{
		if (! isValidVerticalVelocity(velocity)){
	 		throw new IllegalStateException();}
	 	this.currentVerticalVelocity = velocity;
	} 
	
	/**
	 * Return the new vertical velocity for this the game object.
	 * 
	 * @param 	dt
	 * 			The given time in seconds.
	 * @return	If the game object's current vertical position is equal to the minimum vertical position, 
	 * 			then his vertical velocity is 0. Else his new vertical velocity is equal to the 
	 * 			result of the formula.
	 * 			| if (getCurrentVerticalPosition()==getMinimumVerticalPosition())
	 * 			|	then result == 0
	 * 			| else
	 * 			|	then result == (getCurrentVerticalVelocity() + getCurrentVerticalAcceleration()*dt)
	 */
	protected double newVerticalVelocity(double dt){
		double newVerticalVelocity;
		
		newVerticalVelocity = getCurrentVerticalVelocity() + getCurrentVerticalAcceleration()*dt;
		return newVerticalVelocity;
	}
	
	/**
	 * Return the new horizontal velocity of this game object.
	 * 
	 * @param 	dt
	 * 			The given time in seconds.
	 * @return  The new horizontal velocity of the game object is equal to the result of the formula.
	 * 			| result == (getCurrentHorizontalVelocity() + getCurrentHorizontalAcceleration()*dt)
	 */
	protected double newHorizontalVelocity(double dt){
		double newHorizontalVelocity;
		newHorizontalVelocity = getCurrentHorizontalVelocity() + getCurrentHorizontalAcceleration()*dt;
		return newHorizontalVelocity;
	}
	
	/**
	 * Check whether a given horizontal velocity is valid.
	 * 
	 * @param 	horizontalVelocity
	 * 			The velocity that has to be checked.
	 * @return	False if the velocity is lower than the minimum horizontal
	 * 			velocity or larger than the maximum horizontal velocity.
	 * 			| result == (horizontalVelocity < 0) || 
	 * 				(horizontalVelocity > getMaximumHorizontalVelocity())
	 * @return 	True if and only if the velocity is higher or equal to the 
	 * 			minimum horizontal velocity and lower or equal to the maximum
	 * 			horizontal velocity.
	 * 			| result == (horizontalVelocity >= 0) &&
	 * 				(horizontalVelocity <= getMaximumHorizontalVelocity())
	 */
	private boolean isValidHorizontalVelocity(double horizontalVelocity) {
		return (Util.fuzzyGreaterThanOrEqualTo(horizontalVelocity, 0)&& Util.fuzzyLessThanOrEqualTo(horizontalVelocity, getInitialMaximumHorizontalVelocity()));
	}

	private double currentHorizontalVelocity;
	private double currentVerticalVelocity;
	private final double initialHorizontalVelocity;	
	private final double initialVerticalVelocity; 
	private final double initialMaximumHorizontalVelocity;

	
	
	

	
	
	
	// 														################  ACCELERATION
	
	/**
	 * Return the current horizontal acceleration of the game object.
	 */
	@Basic
	public double getCurrentHorizontalAcceleration() {
		return this.currentHorizontalAcceleration;
	}
	
	// 														################  ACCELERATION
	
	/**
	 * Return the current vertical acceleration of the game object.
	 * @return
	 * 		| if (!getWorld().collisionBottom(0, this))
	 * 		|		result == getInitialVerticalAcceleration()
	 * 		| else
	 * 		|		result == 0
	 */
	@Basic
	public double getCurrentVerticalAcceleration(){
		if (!getWorld().collisionBottom(0, this))
			return getInitialVerticalAcceleration();
		return 0;
		}

	/**
	 * Return the initial horizontal acceleration of the game object.
	 * 
	 * @return 	The acceleration that the game object initially gets at a
	 * 			horizontal movement.
	 * 			| result == initialHorizontalAcceleration
	 */
	@Basic @Immutable
	protected double getInitialHorizontalAcceleration(){
		return this.initialHorizontalAcceleration;
	}

	/**
	 * Return the initial vertical acceleration of the game object.
	 * 
	 * @return 	The acceleration that the game object initially gets at a
	 * 			vertical movement.
	 * 			| result == initialFallingAcceleration
	 */
	@Basic @Immutable
	protected double getInitialVerticalAcceleration() {
		return this.initialFallingAcceleration;
	}

	/**
	 * Sets the horizontal acceleration of the game object to the given acceleration.
	 * 
	 * @param 	acceleration
	 * 			The new horizontal acceleration for this the game object.
	 * @post	The new horizontal acceleration of this the game object is equal to 
	 * 			the given acceleration.
	 * 			| new.getCurrentHorizontalAcceleration() == acceleration
	 */
	protected void setHorizontalAcceleration(double acceleration){
		currentHorizontalAcceleration = acceleration; 
	}
	
	/**
	 * Sets the vertical acceleration of the game object to the given acceleration.
	 * 
	 * @param 	acceleration
	 * 			The new vertical acceleration for this the game object.
	 * @post	The new vertical acceleration of this the game object is equal to 
	 * 			the given acceleration.
	 * 			| new.getCurrentVerticalAcceleration() == acceleration
	 */
	protected void setVerticalAcceleration(double acceleration){
		this.currentVerticalAcceleration = acceleration;
	}
	
	private double currentHorizontalAcceleration;
	private double currentVerticalAcceleration;
	private final double initialHorizontalAcceleration;
	private double initialFallingAcceleration;

	
	//														################  SIZE
	
	/**
	 * Return the height of the current sprite image.
	 */
	@Basic
	public int getHeight(){ 
		return getCurrentSprite().getHeight();
	}
	
	/**
	 * Return the width of the current sprite image.
	 * @return
	 */
	@Basic 
	public int getWidth(){
		return getCurrentSprite().getWidth();
	}
	
	
	/**
	 * Returns the sprite of the game object according to its current state.
	 */
	public abstract Sprite getCurrentSprite();
	
	
	
	/**
	 * Return the sprite on a given index in the sprites array.
	 * @param index
	 * 		The index in the array of the wanted sprite.
	 */
	protected Sprite getSprite(int index) {
		return getArrayOfSprites()[index];	
	}

	/**
	 * Return the array with sprite images.
	 */
	protected Sprite[] getArrayOfSprites() {
		return sprites;
	}

	/**
	 * Check whether the given array is a valid array.
	 * 
	 * @param 	array
	 * 			The array that has to be checked.
	 * @return	False if the length of the array isn't an even number.
	 * 			| result == (array.length % 2 != 0)
	 * @return	True if and only if the length of the array is an even number
	 * 			and its length is is a number bigger or equal to 10.
	 * 			| (array.length % 2 == 0)
	 */
	protected boolean isValidArray(Sprite[] array){
		return (array.length % 2 == 0);
	}
	
	private final Sprite[] sprites;
	
	


//		################  MOVEMENT
		
	/**
	* Check whether the game object is moving to the left.
	*/
	@Basic
	public boolean isMovingLeft(){
		return this.isMovingLeft;
	}

	/**
	* Check whether the game object is moving to the right.
	*/
	@Basic
	public boolean isMovingRight() {
		return this.isMovingRight;
	}

	/**
	* Sets the jumping state of this the game object according
	* to the given value.
	* 
	* @param 	value
	* 			The new jumping state of this the game object.
	* @post	The new jumping state of this the game object is 
	* 			equal to the given value.
	* 			| new.isJumping == value
	*/
	public void setJumping(boolean value) {
		this.isJumping = value;
	}

	/**
	* The game object starts jumping if it is allowed to do so.
	* 
	* @effect	
	* 			| if (isAllowedToJump()) {
	* 			| 		setVerticalVelocity(getInitialVerticalVelocity())
	* 			| 		setVerticalAcceleration(getInitialVerticalAcceleration())
	* 			| }
	*/
	public void startJump(){
		if (isAllowedToJump()) {
			setVerticalVelocity(this.getInitialVerticalVelocity());
			setVerticalAcceleration(this.getInitialVerticalAcceleration());
		}
	}
	
	/**
	* the game object stops jumping.
	* 
	* @effect	If the game object's current vertical velocity is larger than 0, his vertical velocity
	* 			is reset.
	* 			| if (getCurrentVerticalVelocity() > 0)
	* 			| 		setVerticalVelocity(0)
	*/
	public void endJump(){
		if (getCurrentVerticalVelocity() > 0)
			setVerticalVelocity(0);
		}

	/**
	* Sets the moving to the left state of this game object according
	* to the given value.
	* 
	* @param 	value
	* 			The new moving to the left state of this the game object.
	* @post	The new moving to the left state of this the game object is 
	* 			equal to the given value.
	* 			| new.isMovingLeft == value
	*/
	public void setMovingLeft(boolean value) {
		this.isMovingLeft = value;
	}

	/**
	* Sets the moving to the right state of this the game object according
	* to the given value.
	* 
	* @param 	value
	* 			The new moving to the right state of this the game object.
	* @post	The new moving to the right state of this the game object is 
	* 			equal to the given value.
	* 			| new.isMovingRight == value
	*/
	public void setMovingRight(boolean value) {
		this.isMovingRight = value;
	}

	/**
	* Check whether the game object is jumping.
	*/
	@Basic
	protected boolean isJumping() {
		return this.isJumping;
	}

	/**
	 * Specifies for a game object if its allowed to jump in the current circumstances.
	 */
	protected abstract boolean isAllowedToJump();
	
	/**
	 * Changes the state of the game object by advancing the time. This eventually might affect position, velocity or acceleration of the game object.
	 * @param dt
	 * 		The amount by which the time is moved forward.
	 */
	protected abstract void advanceTime(double dt);
	
	/**
	 * Slices the time advancement dt to smaller fractions of time to ensure that a game object move approximately 1 pixel per time slice.
	 * @param 	dt
	 * 			The part of time that has to be sliced in smaller fractions
	 * @return 	The smaller fraction of time. 
	 * 			| if (0.01 / (Math.abs(getCurrentVerticalVelocity()) + Math.abs(getCurrentVerticalAcceleration())*dt))
	 * 			|		< (0.01/(getCurrentHorizontalVelocity()+getCurrentHorizontalAcceleration()*dt))
	 * 			|	return (0.01 / (Math.abs(getCurrentVerticalVelocity()) + Math.abs(getCurrentVerticalAcceleration())*dt))
	 * 			| else
	 * 			| 	return (0.01/(getCurrentHorizontalVelocity()+getCurrentHorizontalAcceleration()*dt))
	 */
	protected double splitDt(double dt){		
		double verticalDt = (0.01 / (Math.abs(getCurrentVerticalVelocity()) + Math.abs(getCurrentVerticalAcceleration())*dt));
		double horizontalDt = (0.01/(getCurrentHorizontalVelocity()+getCurrentHorizontalAcceleration()*dt));
		if (verticalDt < horizontalDt){
			return verticalDt;
		}
		else
			return horizontalDt;
	}
	
	
	/**
	 * 
	 * Start the movement of the game object.
	 * 
	 * @effect	The current horizontal velocity is set to the initial horizontal velocity.
	 * 			| setHorizontalVelocity(getInitialHorizontalVelocity())
	 * @effect	The current horizontal acceleration is set to the initial horizontal acceleration.
	 * 			| setHorizontalAcceleration(getInitialHorizontalAcceleration())
	 */
	protected void startMove(){
		this.setHorizontalVelocity(getInitialHorizontalVelocity());
		this.setHorizontalAcceleration(getInitialHorizontalAcceleration());
	}
	
	private boolean isMovingLeft;
	private boolean isMovingRight;
	private boolean isJumping;


	
}
