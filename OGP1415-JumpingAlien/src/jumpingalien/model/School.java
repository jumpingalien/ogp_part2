package jumpingalien.model;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A class representing a school of slimes.
 * 
 * @author 	Bram Vandendriessche
 * @author	Maaike Van Roy
 *
 */
public class School {

	/**
	 * Initialize a school.
	 */
	public School(){
		
	}
	
	/**
	 * Return the size of this school.
	 */
	int schoolSize(){
		return slimes.size();
	}
	
	/**
	 * Add the given slime to this school.
	 * 
	 * @param 	slime
	 * 			The given slime.
	 * @effect	|slimes.add(slime)
	 */
	void addSlime(Slime slime){
		slimes.add(slime);
	}
	
	/**
	 * Apply the effects of a collision to all other slimes in this school.
	 * 
	 * @param 	collisioned
	 * 			The slime that had a collision.
	 * @effect	| for each slime in slimes
	 * 			|	if (slime != collisioned)
	 * 			|		slime.setNbHitPoints(slime.getNbHitPoints() - 1)
	 */
	void collisionEffectOtherSlimes(Slime collisioned){
		for (Slime slime : slimes){
			if (slime != collisioned){
				slime.setNbHitPoints(slime.getNbHitPoints() - 1);
			}
		}
	}
	
	/**
	 * Apply the effects to the old school when a slime changes schools.
	 * 
	 * @param 	changer
	 * 			The slime that changes schools.
	 * @effect	| for each slime in slimes
	 * 			|	if (slime != changer)
	 * 			|		slime.setNbHitPoints(slime.getNbHitPoints() + 1)
	 * 			|	else
	 * 			|		changer.setNbHitPoints(changer.getNbHitPoints() - (schoolSize()-1))
	 */
	void changeOldSchoolEffect(Slime changer){
		for (Slime slime : slimes){
			if (slime != changer){
				slime.setNbHitPoints(slime.getNbHitPoints() + 1);
			}
			else
				changer.setNbHitPoints(changer.getNbHitPoints() - (schoolSize()-1));
		}
	}
	
	/**
	 * Apply the effects to the new school when a slime changes schools.
	 * 
	 * @param 	changer
	 * 			The slime that changes schools.
	 * @effect	| for each slime in slimes
	 * 			|	slime.setNbHitPoints(slime.getNbHitPoints() - 1)
	 * 			| changer.setNbHitPoints(changer.getNbHitPoints() + (schoolSize()-1))
	 */
	void changeNewSchoolEffect(Slime changer){
		for (Slime slime : slimes){
			slime.setNbHitPoints(slime.getNbHitPoints() - 1);
		}
		changer.setNbHitPoints(changer.getNbHitPoints() + (schoolSize()-1));
	}
	
	private Collection<Slime> slimes = new ArrayList<Slime>();
}
