package jumpingalien.model;

import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

/**
 * A class of plants as a specific kind of game objects.
 * 
 * 
 * @author Bram Vandendriessche
 * @author Maaike Van Roy
 *
 */
public class Plant extends GameObject {

	/**
	 * Initialize the new plant with a given horizontal and vertical position, and a list of sprites.
	 * @param x
	 * 		The horizontal position of this plant.
	 * @param y
	 * 		The vertical position of this plant.
	 * @param sprites
	 * 		The array of sprite images for this plant.
	 * @effect
	 * 		|super(x, y, sprites, 1, .5, .5, 0, 0, 0, 1, 0.6)
	 * @effect	
	 * 		|this.setMovingRight(true)
	 * @effect
	 * 		|this.setHorizontalVelocity(getInitialHorizontalVelocity())
	 */
	public Plant(int x, int y, Sprite[] sprites) {
		super(x, y, sprites, 1, .5, .5, 0, 0, 0, 1, 0.6);
		this.setMovingRight(true);
		this.setHorizontalVelocity(getInitialHorizontalVelocity());
	}
	
	// sprites
	
	/**
	 * Check whether the given array is a valid array.
	 * 
	 * @return	False if the length of the array doesn't equal 2.
	 * 			| result == (array.length != 2)
	 * @return	True if and only if the length of the array is equal to 2.
	 * 			| result == (array.length == 2)
	 */
	
	@Override	
	protected boolean isValidArray(Sprite[] array){
		return (array.length == 2);
	}
	
	
	
	/**
	 * Returns the sprite of a plant depending on the direction in which it is moving.
	 * @return
	 * 		| if(isMovingLeft)
	 * 		|	return getSprite(0)
	 * 		| else
	 * 		|	return (getSprite(1)
	 */
	public Sprite getCurrentSprite() {
		if (isMovingLeft())
			return getSprite(0);
		else
			return getSprite(1);
	}
	
	// movement
	
	/**
	 * Returns false.
	 * @return
	 * 		| false
	 */
	@Override
	protected boolean isAllowedToJump() {
		return false;
	}
	
	/**
	 * Returns the local time of this plant.
	 */
	private double getLocalTime() {
		return localTime;
	}
	
	/**
	 * Advances the game time with a certain amount dt to advance the movement of the plant. 
	 *
	 * @effect
	 * 		| if (Util.fuzzyGreaterThanOrEqualTo((getLocalTime + dt), .5)) {
	 * 		|		changeHorizontalPosition(getHorizontalDistanceTravelled(.5 - getLocalTime())
	 * 		|		changeDirection()
	 * 		|		setLocalTime(getLocalTime + dt - .5)
	 * 		|		changeHorizontalPosition(getLocalTime())
	 * 		| }
	 * 		| else {
	 * 		|		changeHorizontalPosition(getHorizontalDistanceTravelled))
	 * 		| 		setLocalTime(getLocalTime() + dt)
	 * 		| }
	 */
	@Override
	protected void advanceTime(double dt) {
		double splitDt = splitDt(dt);
		double smallDt = splitDt;
		while(Util.fuzzyLessThanOrEqualTo(smallDt, dt)){
			double newTime = getLocalTime() + smallDt;
			if (Util.fuzzyGreaterThanOrEqualTo(newTime, .5)) {
				double legalTime = .5 - getLocalTime();
				changeHorizontalPosition(getHorizontalDistanceTravelled(legalTime));
				changeDirection();
				setLocalTime(newTime - .5);
				changeHorizontalPosition(getLocalTime());
			}
			else {
				changeHorizontalPosition(getHorizontalDistanceTravelled(smallDt));
				setLocalTime(getLocalTime() + smallDt);
			}
		smallDt += splitDt;
		}		
	}
	
	
	/**
	 * Set the local time to a given value.
	 * @param value
	 * 		The value to which the local time should be set.
	 * @post
	 * 		| new.getLocalTime() = localTime
	 */
	private void setLocalTime(double value) {
		this.localTime = value;
	}

	/**
	 * Changes the movement direction of the plant from left to right or vice versa.
	 * @effect
	 * 		| setMovingLeft(! isMovingLeft())
	 * @effect
	 * 		| setMovingRight(! isMovingRight())
	 */
	private void changeDirection() {
		setMovingLeft(! isMovingLeft());
		setMovingRight(! isMovingRight());
	}
	
	/**
	 * Move the plant horizontally with a given amount.
	 * @param amount
	 * 		For what amount the plant should be moved.
	 * @effect
	 * 		| if (isMovingRight() && !isTerminated())
	 * 		|		setCurrentHorizontalPosition(getCurrentHorizontalPosition() + amount)
	 * @effect
	 * 		| if (isMovingLeft() && ! isTerminated())
	 * 		| 		setCurrentHorizontalPosition(getCurrentHorizontalPosition() - amount)
	 */
	private void changeHorizontalPosition(double amount) {
		if (isMovingRight() && !isTerminated())
			setCurrentHorizontalPosition(getCurrentHorizontalPosition() + amount);
		if (isMovingLeft() && !isTerminated())
			setCurrentHorizontalPosition(getCurrentHorizontalPosition() - amount);
	}
	
	private double localTime;

		
	// collision
	
	
	/**
	 * Applies effects of collisions of this plant with another object.
	 */
	@Override
	protected void applyCollisionEffects(GameObject object, boolean bottom) {
	}

	/**
	 * Applies effects of a collision between this plant and the environment.
	 */
	@Override
	protected void applyPassableAreaCollisions(double dt) {
	}
	
	
}
