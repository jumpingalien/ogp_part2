package jumpingalien.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import jumpingalien.util.Util;


/**
 * A class representing the game world.
 * 
 * @version	1.0	 
 * @author	Bram Vandendriessche
 * @author	Maaike Van Roy
 *
 */
public class World {

	/**
	 * Initialize this new world with the given tile size, given number of tiles
	 * horizontally, given number of tiles vertically, visible window width and height, 
	 * the horizontal and vertical position of the target tile.
	 * 
	 * @param 	tileSize
	 * 			The given tile size.
	 * @param 	nbTilesX
	 * 			The given number of tiles horizontally.
	 * @param 	nbTilesY
	 * 			The given number of tiles vertically.
	 * @param 	visibleWindowWidth
	 * 			The given width of the visible window.
	 * @param 	visibleWindowHeight
	 * 			The given height of the visible window.
	 * @param 	targetTileX
	 * 			The horizontal position of the target tile.
	 * @param 	targetTileY
	 * 			The vertical position of the target tile.
	 * @effect	| setVisibleWindowX(0)
	 * @effect  | setVisibleWindowY(0)
	 * @post	| new.getNbTilesX() == nbTilesX
	 * @post    | new.getNbTilesY() == nbTilesY
	 * @post	| new.getTileSize() == tileSize
	 * @post    | new.getVisibleWindowWidth() == visibleWindowWidth
	 * @post    | new.getVisibleWindowHeight() == visibleWindowHeight
	 * @post	| new.getTargetTileX() == targetTileX
	 * @post	| new.getTargetTileY() == targetTileY
	 * @post	| new.tiles = new int[nbTilesY][nbTilesX]
	 * 
	 */ 
	public World(int tileSize, int nbTilesX, int nbTilesY,
			int visibleWindowWidth, int visibleWindowHeight, int targetTileX,
			int targetTileY) {
		this.tileSize = tileSize;
		this.nbTilesX = nbTilesX;
		this.nbTilesY = nbTilesY;
		this.tiles = new int[nbTilesY][nbTilesX];
		this.visibleWindowWidth = visibleWindowWidth;
		this.visibleWindowHeigth = visibleWindowHeight;
		setVisibleWindowX(0);
		setVisibleWindowY(0);
		this.targetTileX = targetTileX;
		this.targetTileY = targetTileY;
	}
	
	// collision
	
	
	/**
	 * Returns a list of geological features that are present on a
	 * game object's perimeter.
	 * 
	 * @param 	object
	 * 			The given game object.
	 * @return	| result == getGeologicalFeature(tilesLeft[i][0]*getTileSize(), tilesLeft[i][1]*getTileSize())
	 * 			|			+ getGeologicalFeature(tilesRight[i][0]*getTileSize(), tilesRight[i][1]*getTileSize()))
	 * 			|			+ getGeologicalFeature(tilesTop[i][0]*getTileSize(), tilesTop[i][1]*getTileSize()))
	 * 			|			+ getGeologicalFeature(tilesBottom[i][0]*getTileSize(), tilesBottom[i][1]*getTileSize()))
	 */
	List<Integer> passableAreaCollisionList(GameObject object) {
		  int[][] tilesLeft = getTilePositionsIn((int) object.getCurrentHorizontalPosition(), (int) object.getCurrentVerticalPosition()+1, (int) object.getCurrentHorizontalPosition(), (int) (object.getCurrentVerticalPosition()+object.getHeight()-2));
		  int[][] tilesRight = getTilePositionsIn((int) object.getCurrentHorizontalPosition() + object.getWidth() -1, (int) object.getCurrentVerticalPosition() + 1,(int) object.getCurrentHorizontalPosition() + object.getWidth() -1, (int) (object.getCurrentVerticalPosition()+object.getHeight()-2));
		  int[][] tilesTop = getTilePositionsIn((int) object.getCurrentHorizontalPosition(), (int) (object.getCurrentVerticalPosition()) + object.getHeight() - 1, (int) (object.getCurrentHorizontalPosition() + object.getWidth() - 1), (int) (object.getCurrentVerticalPosition() + object.getHeight() - 1));
		  int[][] tilesBottom = getTilePositionsIn((int) object.getCurrentHorizontalPosition(), (int) object.getCurrentVerticalPosition(), (int) object.getCurrentHorizontalPosition() + object.getWidth()-1, (int) object.getCurrentVerticalPosition());
		  List<Integer> tilesFeatures = new ArrayList<Integer>();
		  
		  for (int i=0; i<tilesLeft.length; i++){
		    tilesFeatures.add(getGeologicalFeature(tilesLeft[i][0]*getTileSize(), tilesLeft[i][1]*getTileSize()));
		  }
		  
		  for (int i=0; i<tilesRight.length; i++){
		    tilesFeatures.add(getGeologicalFeature(tilesRight[i][0]*getTileSize(), tilesRight[i][1]*getTileSize()));
		  }
		  
		  for (int i=0; i<tilesTop.length; i++){
		    tilesFeatures.add(getGeologicalFeature(tilesTop[i][0]*getTileSize(), tilesTop[i][1]*getTileSize()));
		  }
		  
		  for (int i=0; i<tilesBottom.length; i++){
		    tilesFeatures.add(getGeologicalFeature(tilesBottom[i][0]*getTileSize(), tilesBottom[i][1]*getTileSize()));
		  }
		  return tilesFeatures;
	}
	
	/**
	 * Returns true or false according to if there is a collision on the right side of 
	 * the given game object.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @param 	object
	 * 			The given game object.
	 * @return  | result == (tileCollision(Collision.RIGHT, dt, object) || objectCollision(Collision.RIGHT, dt, object))
	 */
	boolean collisionRight(double dt, GameObject object){
		return (tileCollision(Collision.RIGHT, dt, object) || objectCollision(Collision.RIGHT, object));
	}
	
	/**
	 * Returns true or false according to if there is a collision on the left side of 
	 * the given game object.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @param 	object
	 * 			The given game object.
	 * @return  | result == (tileCollision(Collision.LEFT, dt, object) || objectCollision(Collision.LEFT, dt, object))
	 */
	boolean collisionLeft(double dt, GameObject object){
		return (tileCollision(Collision.LEFT, dt, object) || objectCollision(Collision.LEFT, object));
	}
	
	/**
	 * Returns true or false according to if there is a collision on the top side of 
	 * the given game object.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @param 	object
	 * 			The given game object.
	 * @return  | result == (tileCollision(Collision.TOP, dt, object) || objectCollision(Collision.TOP, dt, object))
	 */
	boolean collisionTop(double dt, GameObject object){
		return (tileCollision(Collision.TOP, dt, object) || objectCollision(Collision.TOP, object));
	}
	
	/**
	 * Returns true or false according to if there is a collision on the bottom side of 
	 * the given game object.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @param 	object
	 * 			The given game object.
	 * @return  | result == (tileCollision(Collision.BOTTOM, dt, object) || objectCollision(Collision.BOTTOM, dt, object))
	 */
	boolean collisionBottom(double dt, GameObject object){
		return (tileCollision(Collision.BOTTOM, dt, object) || objectCollision(Collision.BOTTOM, object));
	}
	
	/**
	 * Checks for a collision with impassable tiles in the given direction and with the given 
	 * game object.
	 * 
	 * @param 	collision
	 * 			The given direction.
	 * @param 	dt
	 * 			The given time.
	 * @param 	object
	 * 			The given game object.
	 * @return	| for each feat in tilesFeatures
	 * 			|	if (feat == 1)
	 * 			|		return true
	 *			| return false
	 */
	boolean tileCollision(Collision collision, double dt, GameObject object){
		int[][] tiles = null;
		if (collision == Collision.RIGHT){
			tiles = getTilePositionsIn((int) object.newHorizontalPosition(dt) + object.getWidth() -1, (int) object.getCurrentVerticalPosition() + 1,(int) object.newHorizontalPosition(dt) + object.getWidth() -1, (int) (object.getCurrentVerticalPosition()+object.getHeight()-2));
		}
		else if (collision == Collision.LEFT){
			tiles = getTilePositionsIn((int) object.newHorizontalPosition(dt), (int) object.getCurrentVerticalPosition()+1, (int) object.newHorizontalPosition(dt), (int) (object.getCurrentVerticalPosition()+object.getHeight()-2));
		}
		else if (collision == Collision.BOTTOM){
			if (Util.fuzzyEquals(dt, 0)){
				tiles = getTilePositionsIn((int) object.getCurrentHorizontalPosition(), (int) object.getCurrentVerticalPosition(), (int) object.getCurrentHorizontalPosition() + object.getWidth()-1, (int) object.getCurrentVerticalPosition());
			}
			else{
			tiles = getTilePositionsIn((int) object.getCurrentHorizontalPosition(), (int) object.newVerticalPosition(dt), (int) object.getCurrentHorizontalPosition() + object.getWidth()-1, (int) object.newVerticalPosition(dt));
			}
		}
		else if (collision == Collision.TOP){
			tiles = getTilePositionsIn((int) object.getCurrentHorizontalPosition(), (int) (object.newVerticalPosition(dt)) + object.getHeight() - 1, (int) (object.getCurrentHorizontalPosition() + object.getWidth() - 1), (int) (object.newVerticalPosition(dt) + object.getHeight() - 1));
		}
		
		List<Integer> tilesFeatures = new ArrayList<Integer>();
		
		for (int i=0; i<tiles.length; i++){
			tilesFeatures.add(getGeologicalFeature(tiles[i][0]*getTileSize(), tiles[i][1]*getTileSize())); // 
		}
		
		for (int number : tilesFeatures){ 
			if (number == 1){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * Checks for an object collision in the given direction and with
	 * the given game object and applies the collision effects.
	 * 
	 * @param 	collision
	 * 			The given direction.
	 * @param 	object
	 * 			The given object.
	 * @return	| if (collision)
	 * 			|	return true
	 * 			| return false
	 */
	private boolean objectCollision(Collision collision, GameObject object){
		boolean hasCollision = false;
		
		for (Plant plant : plants){
			if (collidesWith(collision, object, plant)){
				object.applyCollisionEffects(plant, hasCollisionBottom(object, plant));
			}
		}
		
		for (Shark shark : sharks){
			if (collidesWith(collision, object, shark) && (object != shark)){ 
				hasCollision = true;
				object.applyCollisionEffects(shark, hasCollisionBottom(object, shark));
			}
		}
		
		for (Slime slime : slimes){
			if (collidesWith(collision, object, slime) && (object != slime)){ 
				hasCollision = true;
				object.applyCollisionEffects(slime, hasCollisionBottom(object, slime));
			}
		}
		

		if (collidesWith(collision, object, alien) && (object != alien)){ 
			hasCollision = true;
			object.applyCollisionEffects(alien, hasCollisionBottom(object,alien)); 
		}
		
		
		if (hasCollision)
			return true;
		return false;
			
	}
	
	/**
	 * Checks to see if there is a collision in the given direction with
	 * the two given game objects.
	 * 
	 * @param 	collision
	 * 			The given direction.
	 * @param 	object
	 * 			The given object.
	 * @param 	other
	 * 			The other given object.
	 * @return	| if (collision)
	 * 			|	return true
	 * 			| return false
	 */
	private boolean collidesWith(Collision collision, GameObject object, GameObject other){
		if (((int)object.getCurrentHorizontalPosition() + (object.getWidth()) ) < (int) other.getCurrentHorizontalPosition()
				|| ((int)other.getCurrentHorizontalPosition() + (other.getWidth())) < (int)object.getCurrentHorizontalPosition()
				|| ((int)object.getCurrentVerticalPosition() + (object.getHeight())) < (int)other.getCurrentVerticalPosition()
				|| ((int)other.getCurrentVerticalPosition() + (other.getHeight())) < (int)object.getCurrentVerticalPosition() )
			return false;
		
		else {
			if (collision == Collision.BOTTOM){
				return ((int) other.getCurrentVerticalPosition()+other.getHeight()) == ((int) object.getCurrentVerticalPosition()); 
			}			
			else if (collision == Collision.TOP){
				return ((int)object.getCurrentVerticalPosition() + object.getHeight()) == ((int)other.getCurrentVerticalPosition()); 
			}
			else if (collision == Collision.LEFT){
				
				return ((int)other.getCurrentHorizontalPosition() + other.getWidth() ) == ((int)object.getCurrentHorizontalPosition());
			}
			else {
				return ((int)object.getCurrentHorizontalPosition() + object.getWidth() ) == ((int)other.getCurrentHorizontalPosition());
			}
		}

	}
	
	/**
	 * Returns true if there is a collision at the bottom of the given object with
	 * the other given object, false otherwise.
	 * 
	 * @param	object
	 * 			The given object.
	 * @param 	other
	 * 			The other given object.
	 * @return	| if (collision)
	 * 			|	return true
	 * 			| return false
	 */
	private boolean hasCollisionBottom(GameObject object, GameObject other){
		if (((int)other.getCurrentVerticalPosition()+other.getHeight()) == ((int)object.getCurrentVerticalPosition())){
			return true;
		}
		return false;
	}
	
	
	// tiles
	

	/**
	 * Returns the bottom left pixel of the given tile.
	 * 
	 * @param 	tileX
	 * 			The given horizontal tile coordinate.
	 * @param 	tileY
	 * 			The given vertical tile coordinate.
	 * @return	| new int[] {tileX*getTileSize(), tileY*getTileSize()}
	 */
	public int[] getBottomLeftPixelOfTile(int tileX, int tileY) {
		return new int[] {tileX*getTileSize(), tileY*getTileSize()};
	}
	
	/**
	 * Returns the tile positions in the given rectangular region.
	 * 
	 * @param 	pixelLeft
	 * 			The given pixel on the left.
	 * @param 	pixelBottom
	 * 			The given pixel on the bottom.
	 * @param 	pixelRight
	 * 			The given pixel on the right.
	 * @param 	pixelTop
	 * 			The given pixel on the top.
	 * @return  | for (i=0; i<tilesX*tilesY; i++)
	 * 			|	if (a<tilesX)
	 * 			|		tiles[i][0] = (pixelLeft + a*getTileSize())/getTileSize()
	 * 			|		tiles[i][1] = (pixelBottom + b*getTileSize())/getTileSize()
	 * 			|	else
	 * 			|		a = 0
	 *			|		b++
	 *			|		tiles[i][0] = pixelLeft/getTileSize()
	 *			|		tiles[i][1] = (pixelBottom + b*getTileSize())/getTileSize()
	 *			|	a++
	 *			| return tiles	
	 */
	public int[][] getTilePositionsIn(int pixelLeft, int pixelBottom,
			int pixelRight, int pixelTop) {
		pixelLeft = pixelLeft - pixelLeft%getTileSize();
		if (pixelRight%getTileSize()!=0)
			pixelRight = (pixelRight - pixelRight%getTileSize())+getTileSize();
		pixelBottom = pixelBottom - pixelBottom%getTileSize();
		if (pixelTop%getTileSize()!=0)
			pixelTop = pixelTop - pixelTop%getTileSize()+getTileSize();
		int tilesX = (pixelRight - pixelLeft)/getTileSize();
		int tilesY = (pixelTop - pixelBottom)/getTileSize();
		int[][] tiles = new int[tilesX*tilesY][2];
		
		int a = 0;
		int b = 0;
		for (int i=0; i<tilesX*tilesY; i++){
			if (a<tilesX){
				tiles[i][0] = (pixelLeft + a*getTileSize())/getTileSize();
				tiles[i][1] = (pixelBottom + b*getTileSize())/getTileSize();
			}
			else{
				a = 0;
				b++;
				tiles[i][0] = pixelLeft/getTileSize();
				tiles[i][1] = (pixelBottom + b*getTileSize())/getTileSize();
			}
			a++;
			
		}
		return tiles;
	}
	
	/**
	 * Return the geological feature of the given tile.
	 * 
	 * @param	pixelX
	 * 			Horizontal position of the given tile.
	 * @param 	pixelY
	 * 			Vertical position of the given tile.
	 * @return  | tiles[pixelY/getTileSize()][pixelX/getTileSize()]
	 * @throws	| IllegalArgumentException
	 * 			| (pixelX%getTileSize() != 0 || pixelY%getTileSize() != 0)
	 */
	public int getGeologicalFeature(int pixelX, int pixelY) {
		  if (pixelX%getTileSize() != 0 || pixelY%getTileSize() != 0)
		    throw new IllegalArgumentException();
		  return tiles[pixelY/getTileSize()][pixelX/getTileSize()];
	}
	
	/**
	 * Returns the tile size.
	 */
	public int getTileSize(){
		return this.tileSize;
	}
	
	/**
	 * Sets the geological feature of the given tile to the given feature.
	 * 
	 * @param 	tileX
	 * 			Horizontal position of the given tile.
	 * @param 	tileY
	 * 			Vertical position of the given tile.
	 * @param 	tileType
	 * 			The given tile type.
	 * @post	| new.tiles[tileY][tileX] == tileType
	 */
	public void setGeologicalFeature(int tileX, int tileY, int tileType) {
		this.tiles[tileY][tileX] = tileType;
	}

	
	/**
	 * Returns the number of tiles horizontally.
	 */
	int getNbTilesX(){
		return this.nbTilesX;
	}
	
	
	/**
	 * Returns the number of tiles vertically.
	 */
	int getNbTilesY(){
		return this.nbTilesY;
	}
	

	/**
	 * Returns the horizontal position of the target tile.
	 */
	int getTargetTileX(){
		return this.targetTileX;
	}
	

	/**
	 * Returns the vertical position of the target tile.
	 */
	int getTargetTileY(){
		return this.targetTileY;
	}

	
	private final int nbTilesY;
	private final int targetTileX;
	private final int targetTileY;
	private final int tileSize;
	private final int nbTilesX;
	private int[][] tiles;
	
	// window
	
	/**
	 * Returns the bottom left and upper right position of the visible window.
	 * 
	 * @return	| new int[] {getVisibleWindowX(),getVisibleWindowY(),
	 * 			|			(getVisibleWindowX() + getVisibleWindowWidth()), (getVisibleWindowY() + getVisibleWindowHeigth())}
	 */
	public int[] getVisibleWindow() {
		int[] pos = new int [4];
		pos[0] = (int) getVisibleWindowX();
		pos[1] = (int) getVisibleWindowY();
		pos[2] = (int) (getVisibleWindowX() + getVisibleWindowWidth());
		pos[3] = (int) (getVisibleWindowY() + getVisibleWindowHeigth());
		return pos;
	}
	
	/**
	 * Returns the width of the visible window.
	 */
	int getVisibleWindowWidth(){
		return visibleWindowWidth;
	}
	
	/**
	 * Returns the height of the visible window.
	*/
	int getVisibleWindowHeigth(){
		return visibleWindowHeigth;
	}
	
	/**
	 * Returns the horizontal position of the visible window. 
	 */
	double getVisibleWindowX() {
		return visibleWindowX;
	}
	
	/**
	 * Returns the vertical position of the visible window.
	 */
	int getVisibleWindowY() {
		return visibleWindowY;
	}

	/**
	 * Sets the horizontal position of the visibile window to the given position.
	 * 
	 * @param 	visibleWindowX
	 * 			The given position.
	 * @post	| new.getVisibleWindowX() == visibleWindowX
	 */
	void setVisibleWindowX(double visibleWindowX) {
		if(visibleWindowX < 0 || visibleWindowX+getVisibleWindowWidth()>= getGameWorldWidth())
			return;
		this.visibleWindowX = (int) visibleWindowX;
	}

	/**
	 * Sets the vertical position of the visible window to the given position.
	 * 
	 * @param 	visibleWindowY
	 * 			The given position.
	 * @post	| new.getVisibleWindowY() == visibleWindowY
	 */
	void setVisibleWindowY(double visibleWindowY) {
		if (visibleWindowY<0 || visibleWindowY+getVisibleWindowHeigth()>=getGameWorldHeigth())
			return;
		this.visibleWindowY = (int) visibleWindowY;
	}
	
	private double visibleWindowX;
	private final int visibleWindowHeigth;
	private int visibleWindowY;
	private final int visibleWindowWidth;
	
	// gameworld
	
	/**
	 * Returns the width of the game world.
	 */
	public int getGameWorldWidth(){
		return getTileSize() * getNbTilesX();
	} 
	
	/**
	 * Returns the height of the game world.
	 */
	public int getGameWorldHeigth(){
		return getTileSize() * getNbTilesY();
	}
	
	// game objects
	
	/**
	 * Returns a copy of the list of plants.
	 */
	public Collection<Plant> getPlants() {
		return new ArrayList<Plant>(plants);
	}
	
	/**
	 * Returns a copy of the list of sharks.
	 */
	public Collection<Shark> getSharks() {
		return new ArrayList<Shark>(sharks);
	}

	/**
	 * Returns a copy of the list of slimes.
	 */
	public Collection<Slime> getSlimes() {
		return new ArrayList<Slime>(slimes);
	}
	
	/**
	 * Sets the given alien as the player in this game world.
	 * 
	 * @param 	alien
	 * 			The given alien.
	 * @pre		| alien != null	
	 * @post	| new.alien == alien
	 * @effect	| alien.setWorld(this)
	 */
	public void setMazub(Mazub alien) { //checken op nullpointer en dat mazub zn positie wel binnen de wereld ligt
		assert (alien != null);
		this.alien = alien;
		alien.setWorld(this);
	}
	
	/**
	 * Adds a new plant to the game world.
	 * 
	 * @param 	plant
	 * 			The given plant.
	 * @pre		| plant != null
	 * @effect	| if (!gameIsStarted())
	 * 			|	plants.add(plant)
	 * 			|	plant.setWorld(this)
	 */
	public void addPlant(Plant plant) {
		assert(plant != null);
		if (!gameIsStarted()){
			plants.add(plant);
			plant.setWorld(this);
		}
	}
	
	/**
	 * Adds the given shark to the game world.
	 * 
	 * @param 	shark
	 * 			The given shark.
	 * @pre		| shark != null
	 * @effect	| if(!gameIsStarted())
	 * 			|	sharks.add(shark)
	 * 			|	shark.setWorld(this)
	 */
	public void addShark(Shark shark) {
		assert(shark != null);
		if (!gameIsStarted()){
			sharks.add(shark);
			shark.setWorld(this);
		}
	}
	
	/**
	 * Adds the given slime to the game world.
	 * 
	 * @param 	slime
	 * 			The given slime.
	 * @pre		| slime != null
	 * @effect	| if(!gameIsStarted())
	 * 			|	slimes.add(slime)
	 * 			|	slime.setWorld(this)
	 */
	public void addSlime(Slime slime) {
		assert(slime != null);
		if (!gameIsStarted()){
			slimes.add(slime);
			slime.setWorld(this);
		}
	}
	
	public void advanceTime(double dt){
		// invoke iteratively all advanceTime's of game objects inhabiting the world, starting with Mazub
		alien.advanceTime(dt);
		
		for (Plant plant : plants){
			plant.advanceTime(dt);
		}
		
		deathPlants(dt);

		for (Slime slime : slimes){
		    slime.advanceTime(dt);
		}
		
		deathSlimes(dt);
		 
		for (Shark shark : sharks){
			shark.advanceTime(dt);
		}
		
		deathSharks(dt);
	}
	
	/**
	 * Checks for each plant if it is terminated and removes it with delay.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @effect	| for each plant in plants
	 * 			|	if (plant.isTerminated() && Util.fuzzyGreaterThanOrEqualTo(plant.getTimeToLive(), 0))
	 * 			|		plant.setTimeToLive(plant.getTimeToLive() - dt)
	 * 			|	if (plant.isTerminated() && Util.fuzzyLessThanOrEqualTo(plant.getTimeToLive(), 0))
	 * 			|		deathPlants.add(plant)
	 * @effect	| for each plant in deathPlants
	 * 			|	plants.remove(plant)
	 */
	private void deathPlants(double dt){ 
		Collection<Plant> deathPlants = new ArrayList<Plant>();
		for (Plant plant : plants){
			if (plant.isTerminated() && Util.fuzzyGreaterThanOrEqualTo(plant.getTimeToLive(), 0))
				plant.setTimeToLive(plant.getTimeToLive() - dt);
			if (plant.isTerminated() && Util.fuzzyLessThanOrEqualTo(plant.getTimeToLive(), 0))
				deathPlants.add(plant);
		}
		for (Plant plant : deathPlants)
			plants.remove(plant);
	}
	
	/**
	 * Checks for each slime if it is terminated and removes it with delay.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @effect	| for each slime in slimes
	 * 			|	if (slime.isTerminated() && slime.getTimeToLive() > 0)
	 * 			|		slime.setTimeToLive(slime.getTimeToLive() - dt)
	 * 			|	if (slime.isTerminated() && slime.getTimeToLive() <= 0)
	 * 			|		deathSlimes.add(slime)
	 * @effect	| for each slime in deathSlimes
	 * 			|	slimes.remove(slime)
	 */
	private void deathSlimes(double dt){
		Collection<Slime> deathSlimes = new ArrayList<Slime>();
		for (Slime slime : slimes){
			if (slime.isTerminated() && slime.getTimeToLive() > 0)
				slime.setTimeToLive(slime.getTimeToLive() - dt);
			if (slime.isTerminated() && slime.getTimeToLive() <= 0)
				deathSlimes.add(slime);
		}
		for (Slime slime : deathSlimes)
			slimes.remove(slime);
	}
	
	/**
	 * Checks for each shark if it is terminated and removes it with delay.
	 * 
	 * @param 	dt
	 * 			The given time.
	 * @effect	| for each shark in sharks
	 * 			|	if (shark.isTerminated() && shark.getTimeToLive() > 0)
	 * 			|		shark.setTimeToLive(shark.getTimeToLive() - dt)
	 * 			|	if (shark.isTerminated() && shark.getTimeToLive() <= 0)
	 * 			|		deathSharks.add(shark)
	 * @effect	| for each shark in deathSharks
	 * 			|	sharks.remove(shark)
	 */
	private void deathSharks(double dt){
		Collection<Shark> deathSharks = new ArrayList<Shark>();
		for (Shark shark : sharks){
			if (shark.isTerminated() && shark.getTimeToLive() > 0)
				shark.setTimeToLive(shark.getTimeToLive() - dt);
			if (shark.isTerminated() && shark.getTimeToLive() <= 0)
				deathSharks.add(shark);
		}
		for (Shark shark : deathSharks)
			sharks.remove(shark);
	}

	private Mazub alien; 
	private Collection<Slime> slimes = new ArrayList<Slime>();
	private Collection<Shark> sharks = new ArrayList<Shark>();
	//hiervoor moet ge nekeer in het boek kijken daar staat welke methodes ge allemaal moet maken
	private Collection<Plant> plants = new ArrayList<Plant>();

	// game
	
	/**
	 * Returns if the player has won or not.
	 */
	public boolean didPlayerWin() {
		return playerWon;
	}
	
	/**
	 * Returns if the game is over or not.
	 */
	public boolean isGameOver() {
		return gameOver;
	}
	
	/**
	 * Sets to true or false according to if the game is started. 
	 * 
	 * @post	| new.gameIsStarted() == flag
	 */
	public void setGameStarted(boolean flag){
		gameStarted = flag;
	}
	
	/**
	 * Returns true if the game is started, false otherwise. 
	 */
	boolean gameIsStarted(){
		return gameStarted;
	}
	
	/**
	 * Sets the value of winning to the given flag.
	 * 
	 * @param 	playerWon	
	 * 			The given flag.
	 * @post	| new.didPlayerWin() == playerWon
	 */
	void setPlayerWin(boolean playerWon) {
		this.playerWon = playerWon;
	}
	
	/**
	 * Sets the value to the given flag whether the game is over or not.
	 * 
	 * @param 	gameOver
	 * 			The given flag.
	 * @post	| new.isGameOver() == gameOver
	 */
	void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	private boolean gameStarted;
	private boolean gameOver = false;
	private boolean playerWon = false;
}
