package jumpingalien.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

/**
 * A class of Sharks as a kind of GameObject.
 * 
 * @version 1.0
 * @author Bram Vandendriessche
 * @author Maaike Van Roy
 */
public class Shark extends GameObject {

	/**
	 * Initialize this new Shark with the given horizontal position, given vertical
	 * position and given array of sprite images.
	 * 
	 * @param 	x
	 * 			The horizontal position for this new Shark.
	 * @param 	y
	 * 			The vertical position for this new Shark.
	 * @param 	sprites
	 * 			The array of sprite images.
	 * @effect	| super(x, y, sprites, 100, 4, 1, 2, 1.5, -10, 100, 0.6)
	 * @effect	| this.chooseDirection()
	 * @effect	| this.generateMovementDuration()
	 * @effect  | this.setJumpingCounter(0)
	 * @effect  | this.generateVerticalAcceleration()
	 * @effect  | this.startMove()
	 * @effect  | this.startDivingRising()
	 */
	public Shark(int x, int y, Sprite[] sprites) {
		super(x, y, sprites, 100, 4, 1, 2, 1.5, -10, 100, 0.6);
		this.chooseDirection();
		this.generateMovementDuration();
		this.setJumpingCounter(0);
		this.generateVerticalAcceleration();
		this.startMove();
		this.startDivingRising();
	}
	
	// sprites
		
	/**
	 * Check whether the given array is a valid array.
	 * 
	 * @return	False if the length of the array doesn't equal 2.
	 * 			| result == (array.length != 2)
	 * @return	True if and only if the length of the array is equal to 2.
	 * 			| result == (array.length == 2)
	 */
	
	@Override	
	protected boolean isValidArray(Sprite[] array){
		return (array.length == 2);
	}
	
	
	/**
	 * Returns a sprite image according to the Shark's movement direction.
	 * 
	 * @return 	| if (isMovingLeft())
	 * 		  	|	return getSprite(0)
	 * 			| else
	 * 			|	return getSprite(1)
	 */
	public Sprite getCurrentSprite() {
		if (isMovingLeft())
			return getSprite(0);
		else
			return getSprite(1);
	}

	// movement
	

	
	
	/**
	 * Starts a jump.
	 * 
	 * @effect 	| setVerticalVelocity(getInitialVerticalVelocity())
	 * 			| setJumping(true)
	 */
	@Override
	public void startJump(){
			setVerticalVelocity(getInitialVerticalVelocity());
			setJumping(true);
		
	}
	
	/**
	 * Return the current vertical acceleration of the game object.
	 * 
	 * @return	| if (!isSubmerged())
	 * 			|	return getInitialVerticalAcceleration()
	 * 			| return getRiseDiveAcceleration()
	 */
	@Basic  @Override
	public double getCurrentVerticalAcceleration(){
		if (!isSubmerged()) {
			return getInitialVerticalAcceleration(); }
		return getRiseDiveAcceleration();
	}
	
	/**
	 * Checks to see if the given vertical velocity is a valid vertical
	 * velocity and return true of false.
	 * 
	 * @return 	| if (!isJumping())
	 * 			|	return Util.fuzzyLessThanOrEqualTo(verticalVelocity, getInitialVerticalVelocity())
	 * 			| return true
	 */
	@Override
	protected boolean isValidVerticalVelocity(double verticalVelocity) {
		if (! isJumping())
			return Util.fuzzyLessThanOrEqualTo(verticalVelocity, getInitialVerticalVelocity());
		return true;
	}
	
	/**
	 * Returns true of false if the Shark is allowed to Jump.
	 * 
	 * @return 	| if (this.getWorld().collisionBottom(0, this))
	 *			|	return true
	 * @return 	| if (this.bottomIsInWater())
	 *			|	return true
	 * @return 	| return false
	 */
	@Override
	protected boolean isAllowedToJump() {
		if (this.getWorld().collisionBottom(0, this))
			return true;
		if (this.bottomIsInWater()){
			return true;
		}
		return false;
	}
	
	/**
	 * Updates the position, velocity, acceleration, movement direction and action 
	 * of the Shark. 
	 * 
	 * @effect	| applyPassableAreaCollisions(dt)
	 * @effect	| checkImmunity(dt)
	 * @effect	| if (Util.fuzzyGreaterThanOrEqualTo(this.getLocalTime() + dt, this.getMovementDuration()))
	 *			|	this.setLocalTime(0)
	 *			|	this.changeHorizontalLocation(this.getMovementDuration() - this.getLocalTime())
	 *		   	|	this.changeVerticalPosition(getMovementDuration() - this.getLocalTime())
	 *			|	this.chooseDirection()
	 *			| 	this.generateMovementDuration()
	 *			|	this.changeHorizontalLocation(this.getLocalTime() - this.getMovementDuration())
	 *			|	setJumpingCounter(getJumpingCounter() + 1)
	 *			|	if (getJumpingCounter() == getNbOfCyclesBeforeJumping())
	 *			|		setJumpingCounter(0)
	 *			|		generateNbOfCycles()
	 *			|		startJump()
	 *			|   else 
	 *			|		if (isJumping()
	 *			| 			endJump()
	 *			|		startDivingRising()
	 *     		|	changeVerticalPosition(this.getLocalTime() - this.getMovementDuration())
     *       	| else 
	 *			|	changeHorizontalLocation(dt)
	 *			|   changeVerticalPosition(dt)
	 */
	@Override
	protected void advanceTime(double dt) {
		applyPassableAreaCollisions(dt);
		double splitDt = splitDt(dt);
		double smallDt = splitDt;
		checkImmunity(dt);
		while(Util.fuzzyLessThanOrEqualTo(smallDt, dt)){
			setLocalTime(this.getLocalTime() + splitDt);
			if (Util.fuzzyGreaterThanOrEqualTo(this.getLocalTime(), this.getMovementDuration())) { 
		
				double oldTime = this.getLocalTime();
				double oldDuration = this.getMovementDuration();
				this.setLocalTime(0);
				this.changeHorizontalLocation(this.getMovementDuration() - oldTime);
				this.changeVerticalPosition(getMovementDuration() - oldTime);
				this.chooseDirection();
				this.generateMovementDuration();
				this.changeHorizontalLocation(oldTime - oldDuration);
				setJumpingCounter(getJumpingCounter() + 1);
				
				if (getJumpingCounter() >= getNbOfCyclesBeforeJumping() && isAllowedToJump()) {
					setJumpingCounter(0);
					startDivingRising();
					startJump();
				}
				
				else {
					if (isJumping()) {
						endJump();
					}
					startDivingRising();
				}
				changeVerticalPosition(oldTime - oldDuration);
			}
			
			else {
				changeHorizontalLocation(splitDt);
				changeVerticalPosition(splitDt);
			}
		smallDt += splitDt;	
		}
		
	}
	
	/**
	 * Returns the duration of a specific movement of this Shark.
	 */
	private double getMovementDuration() {
		return this.movementDuration;
	}

	
	/**
	 * Returns the local time of this Shark. 
	 */
	private double getLocalTime() {
		return localTime;
	}
	
	/**
	 * Returns an integer that counts how many cycles are in between jumps. 
	 */
	private int getJumpingCounter() {
		return jumpingCounter;
	}
	
	/**
	 * Returns the number a cycles that have to be in between jumps.
	 */
	private int getNbOfCyclesBeforeJumping() {
		return 5;
	}
	
	/**
	 * Returns the acceleration used for rising and diving. 
	 */
	private double getRiseDiveAcceleration() {
		return this.riseDiveAcceleration;
	}
	
	/**
	 * Randomly chooses a movement direction.
	 * 
	 * @effect	| if ((new Random()).nextBoolean())
	 * 			|	setMovingRight(true)
	 * 			|	setMovingLeft(false)
	 * 			| else
	 * 			|	setMovingRight(false)
	 * 			|   setMovingLeft(true)
	 * 			| this.setHorizontalVelocity(getInitialHorizontalVelocity())
	 */
	private void chooseDirection() {
		if ((new Random()).nextBoolean()) {
			setMovingRight(true);
			setMovingLeft(false);
		}
		else{
			setMovingRight(false);
			setMovingLeft(true);
		}
		this.setHorizontalVelocity(getInitialHorizontalVelocity());
			
	}
	
	/**
	 * Changes the horizontal location of this Sharks.
	 * 
	 * @param 	dt
	 * 			The given time duration.
	 * @effect	| if (getWorld() != null && !isTerminated())
	 * 			|	setHorizontalVelocity(newHorizontalVelocity(dt))
	 *			|	if (isMovingLeft() && !(getWorld().collisionLeft(dt, this)))
	 *			|		setCurrentHorizontalPosition(getCurrentHorizontalPosition() - getHorizontalDistanceTravelled(dt))
	 *			|	else if (isMovingRight() && !(getWorld().collisionRight(dt, this)))
	 *			|		setCurrentHorizontalPosition(getCurrentHorizontalPosition() + getHorizontalDistanceTravelled(dt))
	 */
	private void changeHorizontalLocation (double dt) {
		if (getWorld() != null && !isTerminated()) {
			setHorizontalVelocity(newHorizontalVelocity(dt));
			if (isMovingLeft() && !(getWorld().collisionLeft(dt, this)))
				setCurrentHorizontalPosition(getCurrentHorizontalPosition() - getHorizontalDistanceTravelled(dt));
			else if (isMovingRight() && !(getWorld().collisionRight(dt, this)))
				setCurrentHorizontalPosition(getCurrentHorizontalPosition() + getHorizontalDistanceTravelled(dt));
		}
	}
		
	
	/**
	 * Generates a random duration for a movement.
	 * 
	 * @post	| new.getMovementDuration() == 1 + (3) * random.nextDouble()
	 */
	private void generateMovementDuration() {
		Random random = new Random();
		this.movementDuration =  1 + (3) * random.nextDouble();
	}
	
	
	/** 
	 * Sets the local time of this Shark to the given time.
	 * 
	 * @param 	localTime
	 * 			The given time.
	 * @post	| new.getLocalTime() == localTime
	 */
	private void setLocalTime(double localTime) {
		this.localTime = localTime;
	}
	

	/**
	 * Sets the jumping counter to the given value.
	 * 
	 * @param 	jumpingCounter
	 * 			The counter that counts how many cycles are in between jumps.
	 * @post	| new.getJumpingCounter() == jumpingCounter
	 */
	private void setJumpingCounter(int jumpingCounter) {
		this.jumpingCounter = jumpingCounter;
	}



	
	/**
	 * Generates a random vertical acceleration for rising and diving.
	 * 
	 * @post	| new.getRiseDiveAcceleration() == -.2 + (.4) * random.nextDouble()
	 */
	private void generateVerticalAcceleration() {
		Random random = new Random();
		this.riseDiveAcceleration =  -.2 + (.4) * random.nextDouble();
	}
	
	/**
	 * Starts rising and diving.
	 * 
	 * @effect	| generateVerticalAcceleration()
	 *			| this.setVerticalVelocity(0)
	 */
	private void startDivingRising() {
		generateVerticalAcceleration();
	}
	
	/**
	 * Changes the vertical position of this Shark.
	 * 
	 * @param 	dt
	 * 			The given time duration.
	 * @effect	| if ((newVerticalPosition(dt) > getCurrentVerticalPosition() && !(getWorld().collisionTop(dt, this))) || (newVerticalPosition(dt) < getCurrentVerticalPosition() && !(getWorld().collisionBottom(dt, this))))
	 * 			| 	setVerticalVelocity(newVerticalVelocity(dt))
	 *			|	setCurrentVerticalPosition(newVerticalPosition(dt))
	 */
	private void changeVerticalPosition(double dt) {
		if ((newVerticalPosition(dt) > getCurrentVerticalPosition() && !(getWorld().collisionTop(dt, this))) || (newVerticalPosition(dt) < getCurrentVerticalPosition() && !(getWorld().collisionBottom(dt, this)))) {
			setVerticalVelocity(newVerticalVelocity(dt));
			setCurrentVerticalPosition(newVerticalPosition(dt));
		}
		else
			setVerticalVelocity(0);
	}

	private double movementDuration;
	private double riseDiveAcceleration;
	private int jumpingCounter;
	private double localTime;


	
	// collision


	/**
	 * Applies the collision effects of an object collision.
	 * 
	 * @effect	| if (object is Slime)
	 * 			|	if (!this.isImmune())
	 * 			|		setNbHitPoints(getNbHitPoints() - 50)
	 *			|       setImmunity(true)
	 * @effect	| 	if (!object.isImmune())
	 * 			|		object.setNbHitPoints(object.getNbHitPoints() - 50)
	 * 			|		object.getSchool().collisionEffectOtherSlimes(object)
	 * 			|		object.setImmunity(true)
	 */
	@Override
	protected void applyCollisionEffects(GameObject object, boolean bottom) {
		if (object instanceof Slime){
			if (!this.isImmune()){
				setNbHitPoints(getNbHitPoints() - 50);
				setImmunity(true);
			}
			
			if (!object.isImmune()){
				object.setNbHitPoints(object.getNbHitPoints() - 50);
				((Slime) object).getSchool().collisionEffectOtherSlimes((Slime)object);
				object.setImmunity(true);
			}
			
		}
	}


	/**
	 * Apply passable area collision effects.
	 * 
	 * @effect	| if (getFeatureTime() == -1)
	 * 			|	if (feat == 3)
	 * 			|		setFeatureTime(dt);
	 *   		|	    setNbHitPoints(getNbHitPoints() - 50)
	 * 			|		noDamage = false
	 * 			|   else if (feat == 0) 
	 *			|       setFeatureTime(dt)
	 *			|       noDamage = false
	 *			| else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2))
	 *			| 	for each feat in featureList
	 *			|     	if (feat == 0)
	 *       	|			setNbHitPoints(getNbHitPoints() - 6)
	 *       	|			setFeatureTime(getFeatureTime() - .2 + dt)
	 *       	|			noDamage = false
	 *          | else 
	 *			|   setFeatureTime(getFeatureTime() + dt)
	 *			|   noDamage = false
	 * @effect	| if (noDamage == true) 
	 *			|   setFeatureTime(-1)
	 */
	@Override
	protected void applyPassableAreaCollisions(double dt) {
	  List<Integer> featureList = this.getWorld().passableAreaCollisionList(this);
	  //3 is magma
	  //2 is water
	  //1 is solid
	  //0 is lucht
	  boolean noDamage = true;
	  if (getFeatureTime() == -1) {
	    for (Integer feat: featureList) {
	      if (feat == 3) {
	        setFeatureTime(dt);
	        setNbHitPoints(getNbHitPoints() - 50);
	        noDamage = false;
	        break;
	      }
	      else if (feat == 0) {
	        setFeatureTime(dt);
	        noDamage = false;
	        break;
	      }
	    }
	  }

	  else if (Util.fuzzyGreaterThanOrEqualTo(getFeatureTime(), .2)) {

	    for (Integer feat: featureList) {
	      if (feat == 0) {
	        setNbHitPoints(getNbHitPoints() - 6);
	        setFeatureTime(getFeatureTime() - .2 + dt);
	        noDamage = false;
	        break;
	      }
	      else if (feat == 3) {
	        setNbHitPoints(getNbHitPoints() - 50);
	        setFeatureTime(getFeatureTime() - .2 + dt);
	        noDamage = false;
	        break;
	      }
	    }
	  }
	  else {
	    setFeatureTime(getFeatureTime() + dt);
	    noDamage = false;

	  }

	  if (noDamage == true) {
	    setFeatureTime(-1);
	  }
	}
	
	/**
	 * Returns the time this Shark has been on a specific tile feature.
	 */
	private double getFeatureTime() {
		return this.featureTime;
	}
	
	/**
	 * Returns true if the Sharks bottom pixels are overlapping with water.
	 * 
	 * @return	| for each feat in tilesFeatures
	 * 			|	if (feat == 2)
	 * 			|		return true
	 * 			| return false
	 */
	private boolean bottomIsInWater() {
		List<Integer> tilesFeatures = new ArrayList<Integer>();
		int[][] tilesBottom = this.getWorld().getTilePositionsIn((int) this.getCurrentHorizontalPosition(), (int) this.getCurrentVerticalPosition(), (int) this.getCurrentHorizontalPosition() + this.getWidth()-1, (int) this.getCurrentVerticalPosition());
		for (int i=0; i<tilesBottom.length; i++){
		    tilesFeatures.add(this.getWorld().getGeologicalFeature(tilesBottom[i][0]*this.getWorld().getTileSize(), tilesBottom[i][1]*this.getWorld().getTileSize()));
		  }
		for (Integer feat: tilesFeatures) {
			if (feat == 2)
				return true;
		}
		return false;

	}
	
	/**
	 * Returns true if Shark is submerged.
	 * 
	 * @return 	| for each feat in tilesFeatures
	 * 			|	if (feat == 2)
	 * 			|		return true
	 * 			| return false
	 */
	private boolean isSubmerged() {
		List<Integer> tilesFeatures = new ArrayList<Integer>();
		int[][] tilesTop = getWorld().getTilePositionsIn((int) this.getCurrentHorizontalPosition(), (int) (this.getCurrentVerticalPosition()) + this.getHeight() - 1, (int) (this.getCurrentHorizontalPosition() + this.getWidth() - 1), (int) (this.getCurrentVerticalPosition() + this.getHeight() - 1));
		for (int i=0; i<tilesTop.length; i++){
		    tilesFeatures.add(getWorld().getGeologicalFeature(tilesTop[i][0]*getWorld().getTileSize(), tilesTop[i][1]*getWorld().getTileSize()));
		  }
		for (Integer feat: tilesFeatures) {
			if (feat == 2)
				return true;
		}
		return false;
	}

	/**
	 * Sets the feature time to the given time.
	 * 
	 * @param 	time
	 * 			The given time.
	 * @post	| new.getFeatureTime() == time
	 */
	private void setFeatureTime(double time) {
		this.featureTime = time;
	}

	private double featureTime;
	
}
