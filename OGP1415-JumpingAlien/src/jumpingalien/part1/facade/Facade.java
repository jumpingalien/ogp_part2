package jumpingalien.part1.facade;

import jumpingalien.model.Mazub;
import jumpingalien.util.ModelException;
import jumpingalien.util.Sprite;

public class Facade implements IFacade{

	@Override
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite[] sprites) {
		return new Mazub(pixelLeftX, pixelBottomY, sprites);
	}
	
	@Override
	public int[] getLocation(Mazub alien) {		
		int[] currentPosition = new int[2];
		try {
		currentPosition[0] = (int) alien.getCurrentHorizontalPosition();
		currentPosition[1] = (int) alien.getCurrentVerticalPosition();
		} catch(IllegalArgumentException e){
			throw new ModelException("Invalid position");
		}
		return currentPosition; 
	}

	@Override
	public double[] getVelocity(Mazub alien) {
		double [] currentVelocity = new double[2];	
		currentVelocity[0] = alien.getCurrentHorizontalVelocity();
		currentVelocity[1] = alien.getCurrentVerticalVelocity();				
		return currentVelocity;
	}

	@Override
	public double[] getAcceleration(Mazub alien) {
		double [] currentAcceleration = new double[2];
		currentAcceleration[0] = alien.getCurrentHorizontalAcceleration();
		currentAcceleration[1] = alien.getCurrentVerticalAcceleration();
		return currentAcceleration;
	}

	@Override
	public int[] getSize(Mazub alien) {
		int[] currentSize = new int[2];
		currentSize[0] = alien.getWidth();
		currentSize[1] = alien.getHeight();
		return currentSize;
	}

	@Override
	public Sprite getCurrentSprite(Mazub alien) {
		return alien.getCurrentSprite();
	}

	@Override
	public void startJump(Mazub alien) {
		alien.setJumping(true);
		alien.startJump();
	}

	@Override
	public void endJump(Mazub alien) {
		alien.setJumping(false);
		alien.endJump();
	}

	@Override
	public void startMoveLeft(Mazub alien) {
		alien.setLastIsRight(false);
		alien.setMovingRight(false);
		alien.setMovingLeft(true);
		alien.setPressingLeft(true);
		alien.setWalkingCycleIndex(9 + alien.getM());
		alien.startMove();
	}

	@Override
	public void endMoveLeft(Mazub alien) {
		if (alien.isPressingRight() && alien.isMovingLeft()) {
			alien.setMovingRight(true);
			alien.setHasSwitchedDirection(true);
			alien.setWalkingCycleIndex(8);
		}
		else
			alien.setHasSwitchedDirection(false);
		
		alien.setMovingLeft(false);
		alien.setPressingLeft(false);

		
		alien.endMove();
		alien.setLastIsRight(false);
	}

	@Override
	public void startMoveRight(Mazub alien) {
		alien.setLastIsRight(true);
		alien.setMovingLeft(false);
		alien.setMovingRight(true);
		alien.setPressingRight(true);
		alien.setWalkingCycleIndex(8);
		alien.startMove();
	}

	@Override
	public void endMoveRight(Mazub alien) {
		if (alien.isPressingLeft() && alien.isMovingRight()) {
			alien.setMovingLeft(true);
			alien.setHasSwitchedDirection(true);
			alien.setWalkingCycleIndex(9 + alien.getM());

		}
		else
			alien.setHasSwitchedDirection(false);
		
		alien.setMovingRight(false);
		alien.setPressingRight(false);

		alien.endMove();
		alien.setLastIsRight(true);
	}

	@Override
	public void startDuck(Mazub alien) {
		alien.setDucking(true);
		alien.startDuck();
		alien.setPressingDown(true);
	}

	@Override
	public void endDuck(Mazub alien) {
		alien.setDucking(false);
		alien.endDuck();
		alien.setPressingDown(false);
	}

	@Override
	public void advanceTime(Mazub alien, double dt) throws ModelException{
		try {
			 alien.advanceTime(dt);
		} catch(IllegalStateException e) {
			throw new ModelException("Invalid time");
		}
		
	}

}
