package jumpingalien.part1.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import jumpingalien.part1.facade.Facade;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.model.Mazub;
import jumpingalien.util.ModelException;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.*;

public class PartialFacadeTest {

	@Test
	public void startMoveRightCorrect() {
		IFacade facade = new Facade();

		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		facade.advanceTime(alien, 0.1);

		// x_new [m] = 0 + 1 [m/s] * 0.1 [s] + 1/2 0.9 [m/s^2] * (0.1 [s])^2 =
		// 0.1045 [m] = 10.45 [cm], which falls into pixel (10, 0)

		assertArrayEquals(intArray(10, 0), facade.getLocation(alien));
	}

	@Test
	public void startMoveRightMaxSpeedAtRightTime() {
		IFacade facade = new Facade();

		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		// maximum speed reached after 20/9 seconds
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.2 / 9);
		}

		assertArrayEquals(doubleArray(3, 0), facade.getVelocity(alien),
				Util.DEFAULT_EPSILON);
	}

	@Test
	public void testAccellerationZeroWhenNotMoving() {
		IFacade facade = new Facade();

		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien),
				Util.DEFAULT_EPSILON);
	}

	@Test
	public void testWalkAnimationLastFrame() {
		IFacade facade = new Facade();

		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2, 2, 10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);

		facade.startMoveRight(alien);

		facade.advanceTime(alien, 0.005);
		for (int i = 0; i < m; i++) {
			facade.advanceTime(alien, 0.075);
		}

		assertEquals(sprites[8+m], facade.getCurrentSprite(alien));
	}
	
	// Position
	
	@Test
	public void testHorizOutOfBoundLeft(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveLeft(alien);
		facade.advanceTime(alien, 0.195);
		assertEquals(0,facade.getLocation(alien)[0]);
	}
	
	@Test
	public void testHorizOutOfBoundsRight(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		for(int i=0; i<100; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(1023,facade.getLocation(alien)[0]);
	}
	
	@Test
	public void testVertOutOfBound(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		for(int i=0; i<2; i++){
			for(int j=0; j<8; j++){
				facade.advanceTime(alien, 0.1);
			}
			facade.startJump(alien);
		}
		facade.advanceTime(alien, 0.195);
		assertEquals(767,facade.getLocation(alien)[1]);
	}
	
	@Test
	public void testVerticalPosition(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		for(int i=0; i<5; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(275,facade.getLocation(alien)[1]);
		for(int i=0; i<5; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(300,facade.getLocation(alien)[1]);
		
	}
	
	@Test
	public void testHorizontalPositionLowVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(145,facade.getLocation(alien)[0]);
		facade.endMoveRight(alien);
		
		facade.startMoveLeft(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(0,facade.getLocation(alien)[0]);
	}
	
	@Test
	public void testHorizontalPositionMaxVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		for(int i=0; i<100; i++){
			facade.advanceTime(alien, 0.2/9);
		}
		assertEquals(444,facade.getLocation(alien)[0]);
		facade.endMoveRight(alien);
		
		facade.startMoveLeft(alien);
		for(int i=0; i<100; i++){
			facade.advanceTime(alien, 0.2/9);
		}
		assertEquals(0,facade.getLocation(alien)[0]);
		facade.endMoveLeft(alien);
	}
	
	@Test
	public void testHorizontalPositionDucking(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		facade.startDuck(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(100,facade.getLocation(alien)[0]);
		facade.endMoveRight(alien);
		
		facade.startMoveLeft(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(0,facade.getLocation(alien)[0]);
		facade.endMoveLeft(alien);
	}
	
	@Test
	public void testAdvTimeNegativeTime(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		try{
			facade.advanceTime(alien, -1);
			assert false;
		} catch(ModelException e){
			assert true;
		}
	}
	
	@Test
	public void testAdvTimeLargeTime(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		try{
			facade.advanceTime(alien, 1);
			assert false;
		} catch(ModelException e){
			assert true;
		}
	}
	
	// Size
		
	@Test
	public void testSprite0(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		assertArrayEquals(intArray(sprites[0].getWidth(),sprites[0].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite1(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startDuck(alien);
		assertArrayEquals(intArray(sprites[1].getWidth(),sprites[1].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite2(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveRight(alien);
		facade.advanceTime(alien, 0.195);
		facade.endMoveRight(alien);
		assertArrayEquals(intArray(sprites[2].getWidth(),sprites[2].getHeight()),facade.getSize(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(intArray(sprites[0].getWidth(),sprites[0].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite3(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveLeft(alien);
		facade.advanceTime(alien, 0.195);
		facade.endMoveLeft(alien);
		assertArrayEquals(intArray(sprites[3].getWidth(),sprites[3].getHeight()),facade.getSize(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(intArray(sprites[0].getWidth(),sprites[0].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite4(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveRight(alien);
		facade.startJump(alien);
		assertArrayEquals(intArray(sprites[4].getWidth(),sprites[4].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite5(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveLeft(alien);
		facade.startJump(alien);
		assertArrayEquals(intArray(sprites[5].getWidth(),sprites[5].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite6(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startDuck(alien);
		facade.startMoveRight(alien);
		assertArrayEquals(intArray(sprites[6].getWidth(),sprites[6].getHeight()),facade.getSize(alien));
		facade.endMoveRight(alien);
		facade.advanceTime(alien, 0.195);
		assertArrayEquals(intArray(sprites[6].getWidth(),sprites[6].getHeight()),facade.getSize(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(intArray(sprites[1].getWidth(),sprites[1].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSprite7(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startDuck(alien);
		facade.startMoveLeft(alien);
		assertArrayEquals(intArray(sprites[7].getWidth(),sprites[7].getHeight()),facade.getSize(alien));
		facade.endMoveLeft(alien);
		facade.advanceTime(alien, 0.195);
		assertArrayEquals(intArray(sprites[7].getWidth(),sprites[7].getHeight()),facade.getSize(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(intArray(sprites[1].getWidth(),sprites[1].getHeight()),facade.getSize(alien));
	}
	
	@Test
	public void testSpriteWalkingCycleRight(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveRight(alien);
		facade.advanceTime(alien, 0.01);
		assertArrayEquals(intArray(sprites[8].getWidth(),sprites[8].getHeight()),facade.getSize(alien));
		for (int i=0; i<10; i++){
			facade.advanceTime(alien, 0.075);
			assertArrayEquals(intArray(sprites[9+i].getWidth(),sprites[9+i].getHeight()),facade.getSize(alien));
		}
	}
	
	@Test
	public void testSpriteWalkingCycleLeft(){
		IFacade facade = new Facade();
		Sprite[] sprites = spriteArrayForSize(2, 2);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveLeft(alien);
		facade.advanceTime(alien, 0.01);
		assertArrayEquals(intArray(sprites[19].getWidth(),sprites[19].getHeight()),facade.getSize(alien));
		for (int i=0; i<10; i++){
			facade.advanceTime(alien, 0.075);
			assertArrayEquals(intArray(sprites[20+i].getWidth(),sprites[20+i].getHeight()),facade.getSize(alien));
		}
	}
	
	// Velocity
	
	@Test
	public void testVelocityAfterMoveOrNotMoving(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		assertArrayEquals(doubleArray(1.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
		facade.endMoveRight(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
		
		facade.startMoveLeft(alien);
		assertArrayEquals(doubleArray(1.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
		facade.endMoveLeft(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
		
	@Test
	public void testDuckVelocityWhenMovingOrNotMoving(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startDuck(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
		facade.startMoveRight(alien);
		assertArrayEquals(doubleArray(1.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
		facade.advanceTime(alien, 0.195);
		assertArrayEquals(doubleArray(1.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	
	@Test
	public void testCalculationOfHorizVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(doubleArray(1.90,0.0),facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testCalculationOfVerticalVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(doubleArray(0.0,-2.0),facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	
	@Test
	public void testVerticalVelocityStartJump(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		assertArrayEquals(doubleArray(0.0, 8.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testVerticalVelocityAfterJumping(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		for (int i=0; i<10; i++){
			facade.advanceTime(alien, 0.195);
		}
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testVerticalVelocityPositiveWhileGoingUp(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		facade.advanceTime(alien, 0.1);
		assertArrayEquals(doubleArray(0.0, 7.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testVerticalVelocityNegativeWhileGoingDown(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(doubleArray(0.0, -2.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test 
	public void testVerticalVelocityAfterEndJump(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		facade.advanceTime(alien, 0.1);
		facade.endJump(alien);
		assertArrayEquals(doubleArray(0.0,0.0), facade.getVelocity(alien), Util.DEFAULT_EPSILON);
	}
	
	// Acceleration
	
	@Test
	public void testAccelerationWhenMoving(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		assertArrayEquals(doubleArray(0.9, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveRight(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		
		facade.startMoveLeft(alien);
		assertArrayEquals(doubleArray(0.9, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveLeft(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testAccelerationWhenJumpingAndMoving(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		assertArrayEquals(doubleArray(0.0, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.startMoveRight(alien);
		assertArrayEquals(doubleArray(0.9, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveRight(alien);
		assertArrayEquals(doubleArray(0.0, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);	
		
		facade.startJump(alien);
		assertArrayEquals(doubleArray(0.0, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.startMoveLeft(alien);
		assertArrayEquals(doubleArray(0.9, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveLeft(alien);
		assertArrayEquals(doubleArray(0.0, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
	}
	
	@Test
	public void testAccelerationWhenJumpingAndBackOnGround(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startJump(alien);
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.01);
		}
		assertArrayEquals(doubleArray(0.0, -10.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.1);
		}
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);

	}
	
	@Test
	public void testAccelerationWhenDuckingAndMoving(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startDuck(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.startMoveRight(alien);
		assertArrayEquals(doubleArray(0.9, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveRight(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.startMoveRight(alien);
		facade.endDuck(alien);
		assertArrayEquals(doubleArray(0.9, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveRight(alien);
		
		facade.startDuck(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.startMoveLeft(alien);
		assertArrayEquals(doubleArray(0.9, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveLeft(alien);
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.startMoveLeft(alien);
		facade.endDuck(alien);
		assertArrayEquals(doubleArray(0.9, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
	}


	@Test 
	public void testHorizontalAccAtMaxVelocity(){
		IFacade facade = new Facade();
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(2, 2));
		facade.startMoveRight(alien);
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.2 / 9);
		}
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
		facade.endMoveRight(alien);
		
		facade.startMoveLeft(alien);
		for (int i = 0; i < 100; i++) {
			facade.advanceTime(alien, 0.2 / 9);
		}
		assertArrayEquals(doubleArray(0.0, 0.0), facade.getAcceleration(alien), Util.DEFAULT_EPSILON);
	}

	
	// Sprite Images
	
	@Test
	public void testSpriteStanding(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		assertEquals(sprites[0], facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteDuckingNotMoving(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startDuck(alien);
		assertEquals(sprites[1], facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteLastToRightNotDuck(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveRight(alien);
		facade.advanceTime(alien,0.195);
		facade.endMoveRight(alien);
		facade.advanceTime(alien,0.195);
		assertEquals(sprites[2], facade.getCurrentSprite(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(sprites[0],facade.getCurrentSprite(alien));
	}	
	
	@Test
	public void testSpriteLastToLeftNotDuck(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveLeft(alien);
		facade.advanceTime(alien,0.195);
		facade.endMoveLeft(alien);
		facade.advanceTime(alien,0.195);
		assertEquals(sprites[3], facade.getCurrentSprite(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(sprites[0],facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteMovingRightJumpingNotDuck(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveRight(alien);
		facade.startJump(alien);
		assertEquals(sprites[4], facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteMovingLeftJumpingNotDuck(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveLeft(alien);
		facade.startJump(alien);
		assertEquals(sprites[5], facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteDuckingMovingRightOrLastRight(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startDuck(alien);
		facade.startMoveRight(alien);
		assertEquals(sprites[6], facade.getCurrentSprite(alien));
		facade.endMoveRight(alien);
		facade.advanceTime(alien, 0.195);
		assertEquals(sprites[6], facade.getCurrentSprite(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(sprites[1], facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteDuckingMovingLeftOrLastLeft(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startDuck(alien);
		facade.startMoveLeft(alien);
		assertEquals(sprites[7], facade.getCurrentSprite(alien));
		facade.endMoveLeft(alien);
		facade.advanceTime(alien, 0.195);
		assertEquals(sprites[7], facade.getCurrentSprite(alien));
		for(int i=0; i<10; i++){
			facade.advanceTime(alien, 0.1);
		}
		assertEquals(sprites[1], facade.getCurrentSprite(alien));
	}
	
	@Test
	public void testSpriteWalkCycleRight(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveRight(alien);
		assertEquals(sprites[8], facade.getCurrentSprite(alien));
		facade.advanceTime(alien, 0.01);
		assertEquals(sprites[8], facade.getCurrentSprite(alien));
		for (int i=0; i<m; i++){
			facade.advanceTime(alien, 0.075);
			assertEquals(sprites[9+i], facade.getCurrentSprite(alien));
			}	
	}
	
	@Test
	public void testSpriteWalkCycleLeft(){
		IFacade facade = new Facade();
		int m = 10;
		Sprite[] sprites = spriteArrayForSize(2,2,10 + 2 * m);
		Mazub alien = facade.createMazub(0, 0, sprites);
		facade.startMoveLeft(alien);
		assertEquals(sprites[9+m], facade.getCurrentSprite(alien));
		facade.advanceTime(alien, 0.01);
		assertEquals(sprites[9+m], facade.getCurrentSprite(alien));
		for (int i=0; i<m; i++){
			facade.advanceTime(alien, 0.075);
			assertEquals(sprites[10+m+i], facade.getCurrentSprite(alien));
			}		
	}
	
	
}
